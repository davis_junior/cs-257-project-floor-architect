package settings;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

/**
 * ColorContext holds all custom colors used by the application.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ColorContext {

	private Color toolbarColor;
	private Color compositeColor;
	private Color doorColor1;

	public ColorContext() {
		toolbarColor = new Color(Display.getCurrent(), 248, 248, 255);
		compositeColor = new Color(Display.getCurrent(), 248, 248, 255);
		doorColor1 = new Color(Display.getCurrent(), 165, 42, 42);
	}

	/**
	 * Gets the tool bar color
	 * 
	 * @return Color of tool bar
	 */
	public Color getToolbarColor() {
		return toolbarColor;
	}

	/**
	 * Sets the tool bar color.
	 * 
	 * @param toolbarColor Color of tool bar
	 */
	public void setToolbarColor(Color toolbarColor) {
		this.toolbarColor.dispose();
		this.toolbarColor = toolbarColor;
	}

	/**
	 * Gets the composite color.
	 * 
	 * @return Color of composite
	 */
	public Color getCompositeColor() {
		return compositeColor;
	}

	/**
	 * Sets the composite color.
	 * 
	 * @param compositeColor Color of composite
	 */
	public void setCompositeColor(Color compositeColor) {
		this.compositeColor.dispose();
		this.compositeColor = compositeColor;
	}

	/**
	 * Gets the first door color.
	 * 
	 * @return Color of first door
	 */
	public Color getDoorColor1() {
		return doorColor1;
	}

	/**
	 * Sets the first door color.
	 * 
	 * @param doorColor1 Color of first door
	 */
	public void setDoorColor1(Color doorColor1) {
		this.doorColor1.dispose();
		this.doorColor1 = doorColor1;
	}

	/**
	 * Disposed all colors of context.
	 */
	public void dispose() {
		toolbarColor.dispose();
		compositeColor.dispose();
		doorColor1.dispose();
	}

}
