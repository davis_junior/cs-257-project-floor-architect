package settings;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * ToolbarSettings holds mappings of all tool items of the application tool bar.
 * Tool item selection can be queried directly here as well as directly
 * selecting.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ToolbarContext {

	private ToolBar toolbar;
	private Map<String, ToolItem> toolItemMappings;

	public ToolbarContext(ToolBar toolbar) {
		this.toolbar = toolbar;

		toolItemMappings = new HashMap<>();
		initialize();
	}

	/**
	 * Creates initial mapping based on current tool item selections.
	 */
	private void initialize() {
		for (ToolItem toolItem : toolbar.getItems()) {
			toolItemMappings.put((String) toolItem.getData("command"), toolItem);
		}
	}

	/**
	 * Whether the specified tool item is selected or not.
	 * 
	 * @param command Command
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	private boolean getSelection(String command) {
		ToolItem toolItem = toolItemMappings.get(command);
		if (toolItem != null)
			return toolItem.getSelection();

		return false;
	}

	/**
	 * Sets the specified tool item as selected or not.
	 * 
	 * @param command  Command
	 * @param selected Selected
	 */
	private void setSelection(String command, boolean selected) {
		ToolItem toolItem = toolItemMappings.get(command);
		if (toolItem != null)
			toolItem.setSelection(selected);
	}

	/**
	 * Whether show grid is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isShowGrid() {
		return getSelection("show_grid");
	}

	/**
	 * Sets show grid.
	 * 
	 * @param selected Selected
	 */
	public void setShowGrid(boolean selected) {
		setSelection("show_grid", selected);
	}

	/**
	 * Whether snap to grid is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isSnapToGrid() {
		return getSelection("snap_to_grid");
	}

	/**
	 * Sets snap to grid.
	 * 
	 * @param selected Selected
	 */
	public void setSnapToGrid(boolean selected) {
		setSelection("snap_to_grid", selected);
	}

	/**
	 * Whether pan mode is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isPanMode() {
		return getSelection("pan_mode");
	}

	/**
	 * Sets pan mode.
	 * 
	 * @param selected Selected
	 */
	public void setPanMode(boolean selected) {
		setSelection("pan_mode", selected);
	}

	/**
	 * Whether select mode is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isSelectMode() {
		return getSelection("select_mode");
	}

	/**
	 * Sets select mode.
	 * 
	 * @param selected Selected
	 */
	public void setSelectMode(boolean selected) {
		setSelection("select_mode", selected);
	}

	/**
	 * Whether show measurements is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isShowMeasurements() {
		return getSelection("show_measurements");
	}

	/**
	 * Sets show measurements.
	 * 
	 * @param selected Selected
	 */
	public void setShowMeasurements(boolean selected) {
		setSelection("show_measurements", selected);
	}

	/**
	 * Restores default selections.
	 */
	public void restoreDefaults() {
		setPanMode(true);
		setSelectMode(false);
		setShowGrid(true);
		setSnapToGrid(true);
		setShowMeasurements(true);
	}
}
