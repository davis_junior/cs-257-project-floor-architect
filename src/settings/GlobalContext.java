package settings;

import java.text.DecimalFormat;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.swt.graphics.Rectangle;

import coords.DisplayPoint;
import coords.InternalPoint;
import main.Controller;
import objects.ImageObject;
import objects.InternalObject;
import objects.InternalObjectContext;
import objects.Wall;
import objects.WallSegmentObject;
import objects.Window;

/**
 * GlobalContext holds all associated containers and current settings of
 * application.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class GlobalContext {

	public static final DecimalFormat df2 = new DecimalFormat("0.##");

	private Controller controller;
	private ToolbarContext toolbarSettings;
	private ButtonContext buttonSettings;
	private ColorContext colorContext;
	private ImageContext imageContext;
	private FileContext fileContext;

	private Set<InternalObject> selectedObjects;
	private Set<InternalObject> closestObjects;
	// group selection
	private boolean groupSelectionActive;
	private DisplayPoint groupSelectionMouseStart;
	private DisplayPoint groupSelectionMouseEnd;

	// wall creation mouse activated
	private Wall buildWall;
	private boolean buildWallActive;

	// wall object creation mouse activated
	private WallSegmentObject buildWallSegmentObject;
	private boolean buildWallSegmentObjectActive;
	private Wall buildWallSegmentObjectClosestWall;
	private Wall buildWallSegmentObjectAssociatedWall;

	// image object creation mouse activated
	private ImageObject buildImageObject;
	private boolean buildImageObjectActive;

	// pan mouse activated
	private boolean panActive;

	private double zoomFactor;
	private double inverseZoomFactor;
	private int gridInternalWidth;
	private int gridDisplayWidth;

	private InternalPoint basePoint; // internal coordinates of current screen
	private InternalObjectContext objects;

	private boolean ctrlDown;

	// move mode (in selection mode)
	private boolean moveActive;
	private DisplayPoint moveMouseStart;
	private DisplayPoint moveMouseEnd;
	private Set<InternalObject> moveInternalObjects;

	public GlobalContext(Controller controller, ToolbarContext toolbarSettings, ButtonContext buttonSettings,
			ColorContext colorContext, ImageContext imageContext, InternalObjectContext internalObjectContext,
			FileContext fileContext) {

		this.controller = controller;
		this.toolbarSettings = toolbarSettings;
		this.buttonSettings = buttonSettings;
		this.colorContext = colorContext;
		this.imageContext = imageContext;
		this.objects = internalObjectContext;
		this.fileContext = fileContext;

		initialize();
	}

	/**
	 * Sets all global defaults.
	 */
	private void initialize() {
		selectedObjects = new LinkedHashSet<>();
		closestObjects = new LinkedHashSet<>();
		groupSelectionActive = false;
		groupSelectionMouseStart = new DisplayPoint();
		groupSelectionMouseEnd = new DisplayPoint();

		buildWall = new Wall();
		buildWallActive = false;

		buildWallSegmentObject = new Window(new Wall());
		buildWallSegmentObjectActive = false;
		buildWallSegmentObjectClosestWall = new Wall();
		buildWallSegmentObjectAssociatedWall = new Wall();

		buildImageObject = null;
		buildImageObjectActive = false;

		panActive = false;

		zoomFactor = 1;
		inverseZoomFactor = 1 / zoomFactor;
		gridInternalWidth = 25;
		gridDisplayWidth = (int) Math.round(gridInternalWidth * zoomFactor);

		basePoint = new InternalPoint(1000, 1000);

		ctrlDown = false;

		moveActive = false;
		moveMouseStart = new DisplayPoint();
		moveMouseEnd = new DisplayPoint();
		moveInternalObjects = new LinkedHashSet<>();
	}

	/**
	 * Gets Controller.
	 * 
	 * @return Controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * Gets toolbar settings.
	 * 
	 * @return ToolbarContext
	 */
	public ToolbarContext getToolbarSettings() {
		return toolbarSettings;
	}

	/**
	 * Sets toolbar settings.
	 * 
	 * @param toolbarSettings
	 */
	public void setToolbarSettings(ToolbarContext toolbarSettings) {
		this.toolbarSettings = toolbarSettings;
	}

	/**
	 * Gets button settings.
	 * 
	 * @return ButtonContext
	 */
	public ButtonContext getButtonSettings() {
		return buttonSettings;
	}

	/**
	 * Sets button settings.
	 * 
	 * @param buttonSettings ButtonContext
	 */
	public void setButtonSettings(ButtonContext buttonSettings) {
		this.buttonSettings = buttonSettings;
	}

	/**
	 * Gets color settings.
	 * 
	 * @return ColorContext
	 */
	public ColorContext getColorContext() {
		return colorContext;
	}

	/**
	 * Sets color settings.
	 * 
	 * @param colorContext ColorContext
	 */
	public void setColorContext(ColorContext colorContext) {
		this.colorContext = colorContext;
	}

	/**
	 * Gets image settings.
	 * 
	 * @return ImageContext
	 */
	public ImageContext getImageContext() {
		return imageContext;
	}

	/**
	 * Sets image settings.
	 * 
	 * @param imageContext ImageContext
	 */
	public void setImageContext(ImageContext imageContext) {
		this.imageContext = imageContext;
	}

	/**
	 * Gets file settings.
	 * 
	 * @return FileContext
	 */
	public FileContext getFileContext() {
		return fileContext;
	}

	/**
	 * Sets file settings.
	 * 
	 * @param fileContext FileContext
	 */
	public void setFileContext(FileContext fileContext) {
		this.fileContext = fileContext;
	}

	/**
	 * Gets select objects.
	 * 
	 * @return Set of selected objects
	 */
	public Set<InternalObject> getSelectedObjects() {
		return selectedObjects;
	}

	/**
	 * Gets closest objects.
	 * 
	 * @return Set of closest objects.
	 */
	public Set<InternalObject> getClosestObjects() {
		return closestObjects;
	}

	/**
	 * Gets whether group selection is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isGroupSelectionActive() {
		return groupSelectionActive;
	}

	/**
	 * Sets group selection active.
	 * 
	 * @param groupSelectionActive Selected
	 */
	public void setGroupSelectionActive(boolean groupSelectionActive) {
		this.groupSelectionActive = groupSelectionActive;
	}

	/**
	 * Gets group selection mouse start.
	 * 
	 * @return DisplayPoint
	 */
	public DisplayPoint getGroupSelectionMouseStart() {
		return groupSelectionMouseStart;
	}

	/**
	 * Sets group selection mouse start.
	 * 
	 * @param groupSelectionMouseStart DisplayPoint
	 */
	public void setGroupSelectionMouseStart(DisplayPoint groupSelectionMouseStart) {
		this.groupSelectionMouseStart = groupSelectionMouseStart;
	}

	/**
	 * Gets group selection mouse end.
	 * 
	 * @return DisplayPoint
	 */
	public DisplayPoint getGroupSelectionMouseEnd() {
		return groupSelectionMouseEnd;
	}

	/**
	 * Sets group selection mouse end.
	 * 
	 * @param groupSelectionMouseEnd DisplayPoint
	 */
	public void setGroupSelectionMouseEnd(DisplayPoint groupSelectionMouseEnd) {
		this.groupSelectionMouseEnd = groupSelectionMouseEnd;
	}

	/**
	 * Gets the screen bounds of the current group selection.
	 * 
	 * @return Rectangle
	 */
	public Rectangle getGroupSelectionDisplayBounds() {
		int x = groupSelectionMouseStart.getX();
		int y = groupSelectionMouseStart.getY();
		int width = groupSelectionMouseEnd.getX() - x;
		int height = groupSelectionMouseEnd.getY() - y;

		// these could be inverted or negative, so normalization is required for
		// standard rectangle
		if (width < 0) {
			x = x + width;
			width = Math.abs(width);
		}

		if (height < 0) {
			y = y + height;
			height = Math.abs(height);
		}

		return new Rectangle(x, y, width, height);
	}

	/**
	 * Gets the internal bounds of the current group selection.
	 * 
	 * @param globals GlobalContext
	 * @return Rectangle
	 */
	public Rectangle getGroupSelectionInternalBounds(GlobalContext globals) {
		Rectangle displayBounds = getGroupSelectionDisplayBounds();

		DisplayPoint topLeft = new DisplayPoint(displayBounds.x, displayBounds.y);
		DisplayPoint bottomRight = new DisplayPoint(displayBounds.x + displayBounds.width,
				displayBounds.y + displayBounds.height);

		InternalPoint topLeftInternalPoint = topLeft.getInternalPoint(globals);
		InternalPoint bottomRightInternalPoint = bottomRight.getInternalPoint(globals);

		int width = bottomRightInternalPoint.getX() - topLeftInternalPoint.getX();
		int height = bottomRightInternalPoint.getY() - topLeftInternalPoint.getY();

		return new Rectangle(topLeftInternalPoint.getX(), topLeftInternalPoint.getY(), width, height);
	}

	/**
	 * Gets current build wall.
	 * 
	 * @return Wall
	 */
	public Wall getBuildWall() {
		return buildWall;
	}

	/**
	 * Sets current build wall.
	 * 
	 * @param buildWall Wall
	 */
	public void setBuildWall(Wall buildWall) {
		this.buildWall = buildWall;
	}

	/**
	 * Gets whether build wall is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildWallActive() {
		return buildWallActive;
	}

	/**
	 * Sets build wall active.
	 * 
	 * @param buildWallActive Selected
	 */
	public void setBuildWallActive(boolean buildWallActive) {
		this.buildWallActive = buildWallActive;
	}

	/**
	 * Gets build wall segment object.
	 * 
	 * @return WallSegmentObject
	 */
	public WallSegmentObject getBuildWallSegmentObject() {
		return buildWallSegmentObject;
	}

	/**
	 * Sets build wall segment object.
	 * 
	 * @param buildWallSegmentObject WallSegmentObject
	 */
	public void setBuildWallSegmentObject(WallSegmentObject buildWallSegmentObject) {
		this.buildWallSegmentObject = buildWallSegmentObject;
	}

	/**
	 * Gets whether build wall segment object is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildWallSegmentObjectActive() {
		return buildWallSegmentObjectActive;
	}

	/**
	 * Sets build wall segment object active.
	 * 
	 * @param buildWallSegmentObjectActive Selected
	 */
	public void setBuildWallSegmentObjectActive(boolean buildWallSegmentObjectActive) {
		this.buildWallSegmentObjectActive = buildWallSegmentObjectActive;
	}

	/**
	 * Gets build wall segment object closest wall.
	 * 
	 * @return Wall
	 */
	public Wall getBuildWallSegmentObjectClosestWall() {
		return buildWallSegmentObjectClosestWall;
	}

	/**
	 * Sets build wall segment object closest wall.
	 * 
	 * @param buildWallSegmentObjectClosestWall Wall
	 */
	public void setBuildWallSegmentObjectClosestWall(Wall buildWallSegmentObjectClosestWall) {
		this.buildWallSegmentObjectClosestWall = buildWallSegmentObjectClosestWall;
	}

	/**
	 * Gets build wall segment object associated wall.
	 * 
	 * @return Wall
	 */
	public Wall getBuildWallSegmentObjectAssociatedWall() {
		return buildWallSegmentObjectAssociatedWall;
	}

	/**
	 * Sets build wall segment object associated wall.
	 * 
	 * @param buildWallSegmentObjectAssociatedWall Wall
	 */
	public void setBuildWallSegmentObjectAssociatedWall(Wall buildWallSegmentObjectAssociatedWall) {
		this.buildWallSegmentObjectAssociatedWall = buildWallSegmentObjectAssociatedWall;
	}

	/**
	 * Gets build image object.
	 * 
	 * @return ImageObject
	 */
	public ImageObject getBuildImageObject() {
		return buildImageObject;
	}

	/**
	 * Sets build image object.
	 * 
	 * @param buildImageObject ImageObject
	 */
	public void setBuildImageObject(ImageObject buildImageObject) {
		this.buildImageObject = buildImageObject;
	}

	/**
	 * Gets whether build image object is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildImageObjectActive() {
		return buildImageObjectActive;
	}

	/**
	 * Sets build image object active.
	 * 
	 * @param buildImageObjectActive Selected
	 */
	public void setBuildImageObjectActive(boolean buildImageObjectActive) {
		this.buildImageObjectActive = buildImageObjectActive;
	}

	/**
	 * Gets whether pan is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isPanActive() {
		return panActive;
	}

	/**
	 * Sets pan active.
	 * 
	 * @param panActive Selected
	 */
	public void setPanActive(boolean panActive) {
		this.panActive = panActive;
	}

	/**
	 * Gets zoom factor.
	 * 
	 * @return Zoom factor
	 */
	public double getZoomFactor() {
		return zoomFactor;
	}

	/**
	 * Sets zoom factor and other associated factors.
	 * 
	 * @param zoomFactor Zoom factor
	 */
	public void setZoomFactor(double zoomFactor) {
		this.zoomFactor = zoomFactor;
		this.inverseZoomFactor = 1 / zoomFactor;
		this.gridDisplayWidth = (int) Math.round(gridInternalWidth * zoomFactor);
	}

	/**
	 * Gets inverse zoom factor.
	 * 
	 * @return Inverse zoom factor
	 */
	public double getInverseZoomFactor() {
		return inverseZoomFactor;
	}

	/**
	 * Gets grid display width.
	 * 
	 * @return Grid display width
	 */
	public int getGridDisplayWidth() {
		return gridDisplayWidth;
	}

	/**
	 * Gets grid internal width.
	 * 
	 * @return Grid internal width
	 */
	public int getGridInternalWidth() {
		return gridInternalWidth;
	}

	/**
	 * Gets base point.
	 * 
	 * @return InternalPoint
	 */
	public InternalPoint getBasePoint() {
		return basePoint;
	}

	/**
	 * Sets base point.
	 * 
	 * @param basePoint InternalPoint
	 */
	public void setBasePoint(InternalPoint basePoint) {
		this.basePoint = basePoint;
	}

	/**
	 * Gets internal object settings.
	 * 
	 * @return InternalObjectContext
	 */
	public InternalObjectContext getObjects() {
		return objects;
	}

	/**
	 * Sets internal object settings.
	 * 
	 * @param objects InternalObjectContext
	 */
	public void setObjects(InternalObjectContext objects) {
		this.objects = objects;
	}

	/**
	 * Gets whether ctrl key is down or not.
	 * 
	 * @return <code>true</code> if key held, <code>false</code> if not
	 */
	public boolean isCtrlDown() {
		return ctrlDown;
	}

	/**
	 * Sets ctrl key down.
	 * 
	 * @param ctrlDown Key down
	 */
	public void setCtrlDown(boolean ctrlDown) {
		this.ctrlDown = ctrlDown;
	}

	/**
	 * Gets whether move is active or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isMoveActive() {
		return moveActive;
	}

	/**
	 * Sets move active.
	 * 
	 * @param moveActive Selected
	 */
	public void setMoveActive(boolean moveActive) {
		this.moveActive = moveActive;
	}

	/**
	 * Gets move mouse start position.
	 * 
	 * @return DisplayPoint
	 */
	public DisplayPoint getMoveMouseStart() {
		return moveMouseStart;
	}

	/**
	 * Sets move mouse start position.
	 * 
	 * @param moveMouseStart DisplayPoint
	 */
	public void setMoveMouseStart(DisplayPoint moveMouseStart) {
		this.moveMouseStart = moveMouseStart;
	}

	/**
	 * Gets move mouse end position.
	 * 
	 * @return DisplayPoint
	 */
	public DisplayPoint getMoveMouseEnd() {
		return moveMouseEnd;
	}

	/**
	 * Sets move mouse end position.
	 * 
	 * @param moveMouseEnd DisplayPoint
	 */
	public void setMoveMouseEnd(DisplayPoint moveMouseEnd) {
		this.moveMouseEnd = moveMouseEnd;
	}

	/**
	 * Gets internal objects currently being moved.
	 * 
	 * @return Set of InternalObjects
	 */
	public Set<InternalObject> getMoveInternalObjects() {
		return moveInternalObjects;
	}

	/**
	 * Sets moving internal objects.
	 * 
	 * @param moveInternalObjects Set of InternalObjects
	 */
	public void setMoveInternalObjects(Set<InternalObject> moveInternalObjects) {
		this.moveInternalObjects = moveInternalObjects;
	}

	/**
	 * Restores global defaults as well as toolbar and button defaults.
	 */
	public void restoreDefaults() {
		initialize();

		getButtonSettings().restoreDefaults();
		getToolbarSettings().restoreDefaults();
	}
}
