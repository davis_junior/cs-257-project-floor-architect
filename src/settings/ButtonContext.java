package settings;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;

import components.ObjectLibrary;

/**
 * ButtonContext contains all buttons of the application that are not tool bar
 * buttons or menu buttons. The buttons can be queried for selection and set
 * selected.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ButtonContext {

	private ObjectLibrary objectLibrary;
	private Map<String, Button> buttonMappings;

	public ButtonContext(ObjectLibrary objectLibrary) {
		this.objectLibrary = objectLibrary;

		buttonMappings = new HashMap<>();
		initialize();
	}

	/**
	 * Creates button mapping of all application buttons. They are associated by
	 * command.
	 */
	private void initialize() {
		for (Control control : objectLibrary.getChildren()) {
			if (control instanceof Button) {
				buttonMappings.put((String) control.getData("command"), (Button) control);
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					if (groupControl instanceof Button) {
						buttonMappings.put((String) groupControl.getData("command"), (Button) groupControl);
					}
				}
			}
		}
	}

	/**
	 * Gets the current selection of the associated button.
	 * 
	 * @param command Command
	 * @return <code>true</code> if button is selected, <code>false</code> if not
	 */
	private boolean getSelection(String command) {
		Button button = buttonMappings.get(command);
		if (button != null)
			return button.getSelection();

		return false;
	}

	/**
	 * Gets the associated name of the specified button.
	 * 
	 * @param command Command
	 * @return Name of button
	 */
	private String getName(String command) {
		Button button = buttonMappings.get(command);
		if (button != null)
			return (String) button.getData("name");

		return null;
	}

	/**
	 * Gets the associated group name of the specified button.
	 * 
	 * @param command Command
	 * @return Group name
	 */
	private String getGroup(String command) {
		Button button = buttonMappings.get(command);
		if (button != null)
			return (String) button.getData("group");

		return null;
	}

	/**
	 * Gets the associated path of the specified button.
	 * 
	 * @param command Command
	 * @return Path of button
	 */
	private String getPath(String command) {
		Button button = buttonMappings.get(command);
		if (button != null)
			return (String) button.getData("path");

		return null;
	}

	/**
	 * Sets the specified button selected or not.
	 * 
	 * @param command  Command
	 * @param selected Selected
	 */
	private void setSelection(String command, boolean selected) {
		Button button = buttonMappings.get(command);
		if (button != null) {
			if (selected)
				selectButtonOnly(button);
			else
				button.setSelection(false);
		}
	}

	/**
	 * Whether build mode is current selected.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildWallMode() {
		return getSelection("wall_build_mode");
	}

	/**
	 * Sets build mode.
	 * 
	 * @param selected Selected
	 */
	public void setBuildWallMode(boolean selected) {
		setSelection("wall_build_mode", selected);
	}

	/**
	 * Whether build window mode is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildWindowMode() {
		return getSelection("window_build_mode");
	}

	/**
	 * Sets build window mode.
	 * 
	 * @param selected Selected
	 */
	public void setBuildWindowMode(boolean selected) {
		setSelection("window_build_mode", selected);
	}

	/**
	 * Whether build door mode is selected or not.
	 * 
	 * @return <code>true</code> if selected, <code>false</code> if not
	 */
	public boolean isBuildDoorMode() {
		return getSelection("door_build_mode");
	}

	/**
	 * Sets build door mode.
	 * 
	 * @param selected Selected
	 */
	public void setBuildDoorMode(boolean selected) {
		setSelection("door_build_mode", selected);
	}

	/**
	 * Selected the specified button and deselects all others.
	 * 
	 * @param button Button
	 */
	public void selectButtonOnly(Button button) {
		button.setSelection(true);

		for (Control control : objectLibrary.getChildren()) {
			if (control instanceof Button && control != button) {
				((Button) control).setSelection(false);
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					if (groupControl instanceof Button && groupControl != button) {
						((Button) groupControl).setSelection(false);
					}
				}
			}
		}
	}

	/**
	 * Whether any button is currently selected.
	 * 
	 * @return <code>true</code> if any selected, <code>false</code> otherwise
	 */
	public boolean isAnyButtonSelected() {
		for (Control control : objectLibrary.getChildren()) {
			if (control instanceof Button) {
				if (((Button) control).getSelection())
					return true;
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					if (((Button) groupControl).getSelection())
						return true;
				}
			}
		}

		return false;
	}

	/**
	 * Whether any library build object button is selected.
	 * 
	 * @return <code>true</code> if any selected, <code>false</code> otherwise
	 */
	public boolean isAnyObjectBuildMode() {
		return isAnyButtonSelected();
	}

	/**
	 * Whether any library image build object button is selected.
	 * 
	 * @return <code>true</code> if any selected, <code>false</code> otherwise
	 */
	public boolean isImageObjectBuildMode() {
		for (Control control : objectLibrary.getChildren()) {
			if (control instanceof Button) {
				Button button = (Button) control;
				if (button.getSelection() && ((String) button.getData("command")).endsWith("_image_build_mode"))
					return true;
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					if (groupControl instanceof Button) {
						Button button = (Button) groupControl;
						if (button.getSelection() && ((String) button.getData("command")).endsWith("_image_build_mode"))
							return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Deselectes all buttons.
	 */
	public void deselectAll() {
		for (Control control : objectLibrary.getChildren()) {
			if (control instanceof Button) {
				((Button) control).setSelection(false);
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					((Button) groupControl).setSelection(false);
				}
			}
		}
	}

	/**
	 * Restores default selections.
	 */
	public void restoreDefaults() {
		deselectAll();
	}
}
