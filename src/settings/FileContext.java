package settings;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import objects.ImageObject;
import objects.InternalObjectContext;
import objects.Wall;
import objects.WallSegmentObject;

/**
 * FileContext holds the associated file of the current project. Saving and
 * loading of project files is handled here.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class FileContext {

	private File file;
	private InternalObjectContext internalObjectContext;
	private Shell shell;

	public FileContext(InternalObjectContext internalObjectContext, Shell shell) {
		this(internalObjectContext, shell, null);
	}

	public FileContext(InternalObjectContext internalObjectContext, Shell shell, File file) {
		this.file = file;
		this.internalObjectContext = internalObjectContext;
		this.shell = shell;
	}

	/**
	 * Gets project-associated file.
	 * 
	 * @return File
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Sets project-associated file.
	 * 
	 * @param file File
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Saves current project to file.
	 * 
	 * @return <code>true</code> if saved, <code>false</code> if not
	 */
	public boolean save() {
		if (file == null)
			return saveAs();
		else
			return writeFile();
	}

	/**
	 * Saves current project to file, opening a file selection dialog.
	 * 
	 * @return <code>true</code> if saved, <code>false</code> if not
	 */
	public boolean saveAs() {
		String selected = showSaveFileDialog();
		if (selected != null) {
			file = new File(selected);
			return writeFile();
		}

		return false;
	}

	/**
	 * Opens and loads project-file.
	 * 
	 * @return <code>true</code> if successfully opened and loaded,
	 *         <code>false</code> if not
	 */
	public boolean open() {
		String selected = showOpenFileDialog();
		if (selected != null) {
			file = new File(selected);
			return loadFile();
		}

		return false;
	}

	/**
	 * Opens a save file dialog.
	 * 
	 * @return Path of selected file or null if none selected
	 */
	private String showSaveFileDialog() {
		FileDialog fd = new FileDialog(shell, SWT.SAVE);
		fd.setText("Save File");
		fd.setFilterExtensions(new String[] { "*.far" });

		return fd.open();
	}

	/**
	 * Opens a open file dialog.
	 * 
	 * @return Path of selected file or null if none selected.
	 */
	private String showOpenFileDialog() {
		FileDialog fd = new FileDialog(shell, SWT.OPEN);
		fd.setText("Open File");
		fd.setFilterExtensions(new String[] { "*.far" });

		return fd.open();
	}

	/**
	 * Shows a friction message on whether to save the current project to a file or
	 * not.
	 * 
	 * @param title Title of message box
	 * @return SWT return code, YES, NO, or CANCEL
	 */
	public int showSaveFileMessageBox(String title) {
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES | SWT.NO | SWT.CANCEL);
		messageBox.setText(title);
		messageBox.setMessage("Do you want to save changes to current file?");
		return messageBox.open();
	}

	/**
	 * Writes out the current project to the project-file in xml format.
	 * 
	 * @return <code>true</code> if successfully written, <code>false</code>
	 *         otherwise
	 */
	private boolean writeFile() {
		if (!file.exists()) {
			// create new file
			try {
				if (!file.createNewFile())
					return false;
			} catch (IOException e) {
				return false;
			}
		}

		// create xml and write out to file
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(InternalObjectContext.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(internalObjectContext, file);
			// jaxbMarshaller.marshal(internalObjectContext, System.out);
			return true;
		} catch (JAXBException e) {
			return false;
		}
	}

	/**
	 * Loads the project-file into the current project.
	 * 
	 * @return <code>true</code> if successfully loaded, <code>false</code>
	 *         otherwise
	 */
	private boolean loadFile() {
		if (file.exists()) {
			// load xml from file to create java object
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(InternalObjectContext.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				InternalObjectContext newInternalObjectContext = (InternalObjectContext) jaxbUnmarshaller
						.unmarshal(file);

				// clone to existing
				this.internalObjectContext.replaceAll(newInternalObjectContext);

				// associate wall segment objects to respective walls
				for (Wall wall : this.internalObjectContext.getWalls()) {
					for (WallSegmentObject wallSegmentObject : wall.getWallSegmentObjects()) {
						wallSegmentObject.setAssociatedWall(wall);
					}
				}

				// force forward slash image paths (should only be a retrospective case)
				for (ImageObject imageObject : this.internalObjectContext.getImageObjects()) {
					imageObject.setPath(imageObject.getPath().replace("\\", "/"));
				}

				return true;
			} catch (JAXBException e) {
				return false;
			}
		}

		return false;
	}
}
