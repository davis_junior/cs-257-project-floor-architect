package settings;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

/**
 * ImageContext holds all manipulated images of application. It is mainly used
 * for ImageObject images which are cached on zoom changes.
 * 
 * @author D. Kornacki
 * @version 4-29-2019
 */
public class ImageContext {

	private Map<String, Image> originalImages;
	private Map<String, Image> cachedImages;

	public ImageContext() {
		this.originalImages = new HashMap<>();
		this.cachedImages = new HashMap<>();
	}

	/**
	 * Gets original image of the specified path.
	 * 
	 * @param path Path to image
	 * @return Image
	 */
	public Image getOriginalImage(String path) {
		return originalImages.get(path);
	}

	/**
	 * Gets cached image of the specified path.
	 * 
	 * @param path Path to image
	 * @return Image
	 */
	public Image getCachedImage(String path) {
		return cachedImages.get(path);
	}

	/**
	 * Adds an image to image mappings.
	 * 
	 * @param globals GlobalContext
	 * @param path    Path to image
	 */
	public void addImage(GlobalContext globals, String path) {
		if (!originalImages.containsKey(path))
			updateImage(globals, path);
	}

	/**
	 * Removes image from image mappings.
	 * 
	 * @param path Path to image
	 */
	public void removeImage(String path) {
		Image originalImage = originalImages.get(path);
		if (originalImage != null) {
			originalImages.put(path, null);
			originalImage.dispose();
		}

		Image cachedImage = cachedImages.get(path);
		if (cachedImage != null) {
			cachedImages.put(path, null);
			cachedImage.dispose();
		}
	}

	/**
	 * Updates cached images based on current zoom factor. Also retrieves original
	 * image if not found.
	 * 
	 * @param globals GlobalContext
	 * @param path    Path to image
	 */
	public void updateImage(GlobalContext globals, String path) {
		Image originalImage = originalImages.get(path);
		if (originalImage == null) {
			originalImage = new Image(Display.getCurrent(), getClass().getClassLoader().getResourceAsStream(path));
			originalImages.put(path, originalImage);
		}

		Image cachedImage = cachedImages.get(path);
		if (cachedImage != null)
			cachedImage.dispose();

		// scale to specified size
		ImageData imageData = originalImage.getImageData().scaledTo(
				(int) Math.round(originalImage.getBounds().width * globals.getZoomFactor()),
				(int) Math.round(originalImage.getBounds().height * globals.getZoomFactor()));
		cachedImages.put(path, new Image(Display.getCurrent(), imageData));
	}

	/**
	 * Updates all cached images. Also associates orignal images if not already
	 * associated.
	 * 
	 * @param globals GlobalContext.
	 */
	public void updateImages(GlobalContext globals) {
		for (String path : originalImages.keySet()) {
			updateImage(globals, path);
		}
	}

	/**
	 * Disposes all images of this context.
	 */
	public void dispose() {
		for (Image originalImage : originalImages.values())
			originalImage.dispose();

		for (Image cachedImage : cachedImages.values())
			cachedImage.dispose();
	}
}
