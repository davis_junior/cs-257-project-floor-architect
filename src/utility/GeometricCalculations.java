package utility;

import org.eclipse.swt.graphics.Rectangle;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;

/**
 * GeometricCalculations is a utility class containing methods for mathematical
 * and geometric calculations that are used frequently.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class GeometricCalculations {

	/**
	 * Gets distance between two points (line segment length).
	 * 
	 * @param x1 X coordinate of first point
	 * @param y1 Y coordinate of first point
	 * @param x2 X coordinate of second point
	 * @param y2 Y coordinate of second point
	 * @return Distance between two points
	 */
	public static double getDistance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}

	/**
	 * Gets distance between two points (line segment length).
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return Distance between two points
	 */
	public static double getDistance(InternalPoint point1, InternalPoint point2) {
		return getDistance(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Gets distance between two points (line segment length).
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return Distance between two points
	 */
	public static double getDistance(DisplayPoint point1, DisplayPoint point2) {
		return getDistance(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Gets distance in feet between two points.
	 * 
	 * @param point1  First point
	 * @param point2  Second point
	 * @param globals GlobalContext
	 * @return Distance in feet
	 */
	public static double getDistanceInFeet(InternalPoint point1, InternalPoint point2, GlobalContext globals) {
		double internalDistance = getDistance(point1.getX(), point1.getY(), point2.getX(), point2.getY());
		return internalDistance / globals.getGridInternalWidth();
	}

	/**
	 * Gets distance in inches between two points.
	 * 
	 * TODO: Test
	 * 
	 * @param point1  First point
	 * @param point2  Second point
	 * @param globals GlobalContext
	 * @return Distance in inches
	 */
	public static double getDistanceInInches(InternalPoint point1, InternalPoint point2, GlobalContext globals) {
		return getDistanceInFeet(point1, point2, globals) * 12;
	}

	/**
	 * Gets a formatted distance between two points. The format is 2 decimal places.
	 * 
	 * @param point1  First point
	 * @param point2  Second point
	 * @param globals GlobalContext
	 * @return Formatted distance between two points
	 */
	public static String getDistanceString(InternalPoint point1, InternalPoint point2, GlobalContext globals) {
		double feetDistance = getDistanceInFeet(point1, point2, globals);

		int feet = (int) feetDistance; // truncate
		double inches = (feetDistance - feet) * 12;

		return (feet > 0 ? feet + "' " : "") + (inches > 0 ? GlobalContext.df2.format(inches) + "\"" : "");
	}

	/**
	 * Gets angle between two points.
	 * 
	 * @param x1 X coordinate of first point
	 * @param y1 Y coordinate of first point
	 * @param x2 X coordinate of second point
	 * @param y2 Y coordinate of second point
	 * @return Angle
	 */
	public static double getAngle(int x1, int y1, int x2, int y2) {
		return Math.atan2(y2 - y1, x2 - x1);
	}

	/**
	 * Gets angle between two points.
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return Angle
	 */
	public static double getAngle(InternalPoint point1, InternalPoint point2) {
		return getAngle(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Gets angle between two points.
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return Angle
	 */
	public static double getAngle(DisplayPoint point1, DisplayPoint point2) {
		return getAngle(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Gets the closest point on the specified line segment to the specified other
	 * point.
	 * 
	 * Credit:
	 * http://www.java2s.com/Code/Java/2D-Graphics-GUI/Returnsclosestpointonsegmenttopoint.htm
	 * 
	 * @param segX1       X coordinate of first line segment point
	 * @param segY1       Y coordinate of first line segment point
	 * @param segX2       X coordinate of second line segment point
	 * @param segY2       Y coordinate of second line segment point
	 * @param otherPointX X coordinate of other point
	 * @param otherPointY Y coordinate of other point
	 * @return Closest point as int array of length 2 where index 0 is x and index 1
	 *         is y
	 */
	public static int[] getClosestPointOnSegment(int segX1, int segY1, int segX2, int segY2, int otherPointX,
			int otherPointY) {
		int xSegDiff = segX2 - segX1;
		int ySegDiff = segY2 - segY1;

		if (xSegDiff == 0 && ySegDiff == 0)
			return new int[] { segX1, segY1 };

		double u = ((otherPointX - segX1) * xSegDiff + (otherPointY - segY1) * ySegDiff)
				/ (double) (xSegDiff * xSegDiff + ySegDiff * ySegDiff);

		// return closest point
		if (u < 0)
			return new int[] { segX1, segY1 };
		else if (u > 1)
			return new int[] { segX2, segY2 };
		else
			return new int[] { (int) Math.round(segX1 + u * xSegDiff), (int) Math.round(segY1 + u * ySegDiff) };
	}

	/**
	 * Gets the closest point on the specified line segment to the specified other
	 * point.
	 * 
	 * @param segPoint1  First line segment point
	 * @param segPoint2  Second line segment point
	 * @param otherPoint Other point
	 * @return Closest point
	 */
	public static InternalPoint getClosestPointOnSegment(InternalPoint segPoint1, InternalPoint segPoint2,
			InternalPoint otherPoint) {

		int[] closestPoint = GeometricCalculations.getClosestPointOnSegment(segPoint1.getX(), segPoint1.getY(),
				segPoint2.getX(), segPoint2.getY(), otherPoint.getX(), otherPoint.getY());

		return new InternalPoint(closestPoint[0], closestPoint[1]);
	}

	/**
	 * Gets the closest point on the specified line segment to the specified other
	 * point.
	 * 
	 * @param segPoint1  First line segment point
	 * @param segPoint2  Second line segment point
	 * @param otherPoint Other point
	 * @return Closest point
	 */
	public static DisplayPoint getClosestPointOnSegment(DisplayPoint segPoint1, DisplayPoint segPoint2,
			DisplayPoint otherPoint) {

		int[] closestPoint = GeometricCalculations.getClosestPointOnSegment(segPoint1.getX(), segPoint1.getY(),
				segPoint2.getX(), segPoint2.getY(), otherPoint.getX(), otherPoint.getY());

		return new DisplayPoint(closestPoint[0], closestPoint[1]);
	}

	/**
	 * A line is straight if slope is 0 or infinity in either direction. This method
	 * tests based on angle. A perfectly vertical or horizontal line is straight.
	 * 
	 * @param x1 X coordinate of first point
	 * @param y1 Y coordinate of first point
	 * @param x2 X coordinate of second point
	 * @param y2 Y coordinate of second point
	 * @return <code>true</code> if straight, <code>false</code> if not straight
	 */
	public static boolean isStraight(int x1, int y1, int x2, int y2) {
		double angleDegs = Math.abs(Math.toDegrees(getAngle(x1, y1, x2, y2)));
		return angleDegs == 0 || angleDegs == 90 || angleDegs == 180 || angleDegs == 270 || angleDegs == 360;
	}

	/**
	 * A line is straight if slope is 0 or infinity in either direction. This method
	 * tests based on angle. A perfectly vertical or horizontal line is straight.
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return <code>true</code> if straight, <code>false</code> if not straight
	 */
	public static boolean isStraight(InternalPoint point1, InternalPoint point2) {
		return isStraight(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * A line is straight if slope is 0 or infinity in either direction. This method
	 * tests based on angle. A perfectly vertical or horizontal line is straight.
	 * 
	 * @param point1 First point
	 * @param point2 Second point
	 * @return <code>true</code> if straight, <code>false</code> if not straight
	 */
	public static boolean isStraight(DisplayPoint point1, DisplayPoint point2) {
		return isStraight(point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Checks if specified line segment intersects interior of specified rectangle.
	 * 
	 * @param rectangle Rectangle
	 * @param point1    First point of line segment
	 * @param point2    Second point of line segment
	 * @return <code>true</code> if line segment intersects interior of rectangle,
	 *         <code>false</code> otherwise
	 */
	public static boolean intersects(Rectangle rectangle, InternalPoint point1, InternalPoint point2) {
		return intersects(rectangle, point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Checks if specified line segment intersects interior of specified rectangle.
	 * 
	 * @param rectangle Rectangle
	 * @param point1    First point of line segment
	 * @param point2    Second point of line segment
	 * @return <code>true</code> if line segment intersects interior of rectangle,
	 *         <code>false</code> otherwise
	 */
	public static boolean intersects(Rectangle rectangle, DisplayPoint point1, DisplayPoint point2) {
		return intersects(rectangle, point1.getX(), point1.getY(), point2.getX(), point2.getY());
	}

	/**
	 * Checks if specified line segment intersects interior of specified rectangle.
	 * 
	 * @param rectangle Rectangle
	 * @param x1        X coordinate of first point
	 * @param y1        Y coordinate of first point
	 * @param x2        X coordinate of second point
	 * @param y2        Y coordinate of second point
	 * @return <code>true</code> if line segment intersects interior of rectangle,
	 *         <code>false</code> otherwise
	 */
	public static boolean intersects(Rectangle rectangle, int x1, int y1, int x2, int y2) {
		// wrap swt rectangle in awt rect
		java.awt.Rectangle awtRect = new java.awt.Rectangle(rectangle.x, rectangle.y, rectangle.width,
				rectangle.height);
		return awtRect.intersectsLine(x1, y1, x2, y2);
	}
}
