package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.swt.graphics.Rectangle;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;

/**
 * RectangleObject is represented internally as a rectangle.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class RectangleObject extends InternalObject {

	@XmlElement(name = "Point")
	protected InternalPoint point;

	@XmlElement(name = "Width")
	protected int width;

	@XmlElement(name = "Height")
	protected int height;

	public RectangleObject(InternalPoint point, int width, int height) {
		super();

		this.point = new InternalPoint(point);
		this.width = width;
		this.height = height;
	}

	/**
	 * Gets the main point.
	 * 
	 * @return Point
	 */
	public InternalPoint getPoint() {
		return point;
	}

	/**
	 * Sets the main point
	 * 
	 * @param point Point
	 */
	public void setPoint(InternalPoint point) {
		this.point = new InternalPoint(point);
	}

	/**
	 * Gets the width.
	 * 
	 * @return Width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width.
	 * 
	 * @param width Width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Gets the height.
	 * 
	 * @return Height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height.
	 * 
	 * @param height Height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Gets the bounds in InternalPoint format.
	 * 
	 * @return Internal bounds
	 */
	public Rectangle getInternalBounds() {
		return new Rectangle(point.getX(), point.getY(), width, height);
	}

	/**
	 * Gets the bounds in DisplayPoint format.
	 * 
	 * @param globals GlobalContext
	 * @return Display bounds
	 */
	public Rectangle getDisplayBounds(GlobalContext globals) {
		InternalPoint topLeft = new InternalPoint(point);
		InternalPoint bottomRight = new InternalPoint(point.getX() + width, point.getY() + height);

		DisplayPoint topLeftDisplayPoint = topLeft.getDisplayPoint(globals);
		DisplayPoint bottomRightDisplayPoint = bottomRight.getDisplayPoint(globals);

		int width = bottomRightDisplayPoint.getX() - topLeftDisplayPoint.getX();
		int height = bottomRightDisplayPoint.getY() - topLeftDisplayPoint.getY();

		return new Rectangle(topLeftDisplayPoint.getX(), topLeftDisplayPoint.getY(), width, height);
	}
}
