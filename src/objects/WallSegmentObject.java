package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import coords.InternalPoint;

/**
 * WallSegmentObject is represented internally as a line segment. It is an
 * object that can be placed on walls.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class WallSegmentObject extends InternalSegmentObject {

	@XmlTransient
	private Wall associatedWall;

	public WallSegmentObject(Wall associatedWall) {
		super();
		this.associatedWall = associatedWall;
	}

	public WallSegmentObject(Wall associatedWall, InternalPoint point1, InternalPoint point2) {
		super(point1, point2);
		this.associatedWall = associatedWall;
	}

	/**
	 * Gets the wall associated with this segment.
	 * 
	 * @return Wall
	 */
	public Wall getAssociatedWall() {
		return associatedWall;
	}

	/**
	 * Sets the wall associated with this segment.
	 * 
	 * @param associatedWall Wall
	 */
	public void setAssociatedWall(Wall associatedWall) {
		this.associatedWall = associatedWall;
	}
}