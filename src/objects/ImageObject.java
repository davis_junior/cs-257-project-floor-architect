package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;

/**
 * ImageObject is represented internally as a rectangle drawn as an image.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "ImageObject")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageObject extends RectangleObject {

	@XmlElement(name = "Name")
	private String name;

	@XmlElement(name = "Path")
	private String path;

	public ImageObject() {
		super(new InternalPoint(), 0, 0);
		this.name = null;
		this.path = null;
	}

	public ImageObject(GlobalContext globals, String name, String path, InternalPoint point) {
		super(point, 0, 0);

		this.name = name;
		this.path = path;

		globals.getImageContext().addImage(globals, path);
		Rectangle imageBounds = globals.getImageContext().getOriginalImage(path).getBounds();
		this.width = imageBounds.width;
		this.height = imageBounds.height;
	}

	/**
	 * Gets name.
	 * 
	 * @return Name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets name.
	 * 
	 * @param name Name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets image path.
	 * 
	 * @return Path to image
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets image path.
	 * 
	 * @param path Path to image
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Draws image.
	 * 
	 * @param gc       Graphics object
	 * @param globals  GlobalContext
	 * @param selected If the image should be drawn as selected or not
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		draw(gc, globals, selected, Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
	}

	/**
	 * Draws image.
	 * 
	 * @param gc             Graphics object
	 * @param globals        GlobalContext
	 * @param selected       If the image should be draw as selected or not
	 * @param selectionColor Color or selection if specified selected
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected, Color selectionColor) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		DisplayPoint displayPoint = point.getDisplayPoint(globals);
		Image cachedImage = globals.getImageContext().getCachedImage(path);
		gc.drawImage(cachedImage, displayPoint.getX(), displayPoint.getY());

		// draw transparent overlay to denote selection
		if (selected) {
			try {
				gc.setAlpha(60);
			} catch (SWTException ex) {
			}

			gc.setBackground(selectionColor);
			gc.setForeground(selectionColor);
			gc.fillRectangle(getDisplayBounds(globals));
		}
	}

	/**
	 * Creates a deep copy of this ImageObject.
	 * 
	 * @param globals GlobalContext
	 * @return Copy of this ImageObject
	 */
	public ImageObject makeClone(GlobalContext globals) {
		return new ImageObject(globals, name, path, point);
	}
}
