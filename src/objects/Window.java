package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;

/**
 * Window is internally represented as a line segment. It can be placed on
 * walls.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "Window")
@XmlAccessorType(XmlAccessType.FIELD)
public class Window extends WallSegmentObject {

	public Window() {
		super(null);
	}

	public Window(Wall associatedWall) {
		super(associatedWall);
	}

	public Window(Wall associatedWall, InternalPoint point1, InternalPoint point2) {
		super(associatedWall, point1, point2);
	}

	/**
	 * Draws a line with specifications.
	 * 
	 * @param gc        Graphics object
	 * @param globals   GlobalContext
	 * @param color     Color of line
	 * @param lineWidth Width of line
	 */
	private void drawLineLayer(GC gc, GlobalContext globals, Color color, int lineWidth) {
		gc.setForeground(color);
		gc.setLineWidth((int) (lineWidth * globals.getZoomFactor()));

		DisplayPoint displayPoint1 = point1.getDisplayPoint(globals);
		DisplayPoint displayPoint2 = point2.getDisplayPoint(globals);

		gc.drawLine(displayPoint1.getX(), displayPoint1.getY(), displayPoint2.getX(), displayPoint2.getY());
	}

	/**
	 * Draws window. If specified selected, the window is drawn green.
	 * 
	 * @param gc       Graphics object
	 * @param globals  GlobalContext
	 * @param selected Whether the window should be drawn as selected or not
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		draw(gc, globals, selected, Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
	}

	/**
	 * Draws window. If specified selected, the window is drawn as the specified
	 * color.
	 * 
	 * @param gc             Graphics object
	 * @param globals        GlobalContext
	 * @param selected       Whether the window should be drawn as selected or not
	 * @param selectionColor Color to drawn window if specified as selected
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected, Color selectionColor) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_BLACK), 16);
		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_DARK_CYAN), 12);
		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_CYAN), 8);

		// draw transparent overlay to denote selection
		if (selected) {
			try {
				gc.setAlpha(128);
			} catch (SWTException ex) {
			}

			drawLineLayer(gc, globals, selectionColor, 16);
		}
	}

	/**
	 * Creates a deep clone of this window.
	 * 
	 * @param associatedWall Wall
	 * @return Deep clone of this window
	 */
	public Window makeClone(Wall associatedWall) {
		return new Window(associatedWall, point1, point2);
	}
}
