package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import coords.InternalPoint;
import utility.GeometricCalculations;

/**
 * InternalSegmentObjects is represented as a line segment.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class InternalSegmentObject extends InternalObject {

	@XmlElement(name = "Point1")
	protected InternalPoint point1;

	@XmlElement(name = "Point2")
	protected InternalPoint point2;

	public InternalSegmentObject() {
		point1 = new InternalPoint();
		point2 = new InternalPoint();
	}

	public InternalSegmentObject(InternalPoint point1, InternalPoint point2) {
		this.point1 = new InternalPoint(point1);
		this.point2 = new InternalPoint(point2);
	}

	/**
	 * Gets the first point.
	 * 
	 * @return First point
	 */
	public InternalPoint getPoint1() {
		return point1;
	}

	/**
	 * Sets the first point.
	 * 
	 * @param point1 First point
	 */
	public void setPoint1(InternalPoint point1) {
		this.point1 = new InternalPoint(point1);
	}

	/**
	 * Gets the second point.
	 * 
	 * @return Second point
	 */
	public InternalPoint getPoint2() {
		return point2;
	}

	/**
	 * Sets the second point.
	 * 
	 * @param point2 Second point
	 */
	public void setPoint2(InternalPoint point2) {
		this.point2 = new InternalPoint(point2);
	}

	/**
	 * Gets the angle of the two points.
	 * 
	 * @return Angle of the two points.
	 */
	public double getAngle() {
		return GeometricCalculations.getAngle(point1, point2);
	}

	/**
	 * If the line segment is straight (perfectly vertical or horizontal).
	 * 
	 * @return <code>true</code> if line segment is straight, <code>false</code>
	 *         otherwise
	 */
	public boolean isStraight() {
		return GeometricCalculations.isStraight(point1, point2);
	}

}
