package objects;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import settings.GlobalContext;

/**
 * InternalObject is the root type of internal object of the application.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class InternalObject {

	/**
	 * Clones the specified InternalObjects. This method attempts to associate
	 * cloned WallSegmentObjects with their cloned wall if in the collection,
	 * otherwise they are associated with their original wall.
	 * 
	 * @param globals         GlobalContext
	 * @param internalObjects InternalObjects
	 * @return A deep copy of the specified InternalObjects
	 */
	public static Set<InternalObject> cloneAll(GlobalContext globals, Collection<InternalObject> internalObjects) {
		if (internalObjects == null)
			return null;

		Map<Wall, Wall> originalToClonedWalls = new HashMap<>();
		Set<WallSegmentObject> wallSegmentsToClone = new LinkedHashSet<>();

		Set<InternalObject> cloned = new LinkedHashSet<>();
		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof Wall) {
				Wall originalWall = (Wall) internalObject;
				Wall clonedWall = originalWall.makeClone(true);
				cloned.add(clonedWall);
				originalToClonedWalls.put(originalWall, clonedWall);
			} else if (internalObject instanceof WallSegmentObject) {
				WallSegmentObject wallSegmentObject = (WallSegmentObject) internalObject;
				if (internalObjects.contains(wallSegmentObject.getAssociatedWall())) {
					// wait for all walls to be cloned first to associate with cloned wall
					// rather than original
					wallSegmentsToClone.add(wallSegmentObject);
					continue;
				} else {
					if (internalObject instanceof Window) {
						Window window = (Window) internalObject;
						cloned.add(window.makeClone(window.getAssociatedWall()));
					} else if (internalObject instanceof Door) {
						Door door = (Door) internalObject;
						cloned.add(door.makeClone(door.getAssociatedWall()));
					}
				}
			} else if (internalObject instanceof ImageObject) {
				ImageObject imageObject = (ImageObject) internalObject;
				cloned.add(imageObject.makeClone(globals));
			}
		}

		for (WallSegmentObject wallSegmentObject : wallSegmentsToClone) {
			if (wallSegmentObject instanceof Window) {
				Window window = (Window) wallSegmentObject;
				cloned.add(window.makeClone(originalToClonedWalls.get(window.getAssociatedWall())));
			} else if (wallSegmentObject instanceof Door) {
				Door door = (Door) wallSegmentObject;
				cloned.add(door.makeClone(originalToClonedWalls.get(door.getAssociatedWall())));
			}
		}

		return cloned;
	}
}
