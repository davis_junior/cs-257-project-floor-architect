package objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;

/**
 * Door is represented internally as a line segment.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "Door")
@XmlAccessorType(XmlAccessType.FIELD)
public class Door extends WallSegmentObject {

	public Door() {
		super(null);
	}

	public Door(Wall associatedWall) {
		super(associatedWall);
	}

	public Door(Wall associatedWall, InternalPoint point1, InternalPoint point2) {
		super(associatedWall, point1, point2);
	}

	/**
	 * Draw a line with specifications.
	 * 
	 * @param gc        Graphics object
	 * @param globals   GlobalContext
	 * @param color     Color
	 * @param lineWidth Width of line
	 */
	private void drawLineLayer(GC gc, GlobalContext globals, Color color, int lineWidth) {
		gc.setForeground(color);
		gc.setLineWidth((int) (lineWidth * globals.getZoomFactor()));

		DisplayPoint displayPoint1 = point1.getDisplayPoint(globals);
		DisplayPoint displayPoint2 = point2.getDisplayPoint(globals);

		gc.drawLine(displayPoint1.getX(), displayPoint1.getY(), displayPoint2.getX(), displayPoint2.getY());
	}

	/**
	 * Draw the door.
	 * 
	 * @param gc       Graphics object
	 * @param globals  GlobalContext
	 * @param selected If the door should be drawn as selected or not
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		draw(gc, globals, selected, Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
	}

	/**
	 * Draw the door according to specifications.
	 * 
	 * @param gc             Graphics object
	 * @param globals        GlobalContext
	 * @param selected       If the door should be drawn as selected or not
	 * @param selectionColor Color to draw with if selected
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected, Color selectionColor) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_BLACK), 16);
		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_DARK_YELLOW), 12);
		drawLineLayer(gc, globals, globals.getColorContext().getDoorColor1(), 8);

		// draw transparent overlay to denote selection
		if (selected) {
			try {
				gc.setAlpha(128);
			} catch (SWTException ex) {
			}

			drawLineLayer(gc, globals, selectionColor, 16);
		}
	}

	/**
	 * Creates a deep copy of this door.
	 * 
	 * @param associatedWall Associated wall
	 * @return Copy of this door
	 */
	public Door makeClone(Wall associatedWall) {
		return new Door(associatedWall, point1, point2);
	}
}
