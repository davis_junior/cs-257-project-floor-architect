package objects;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

import coords.DisplayPoint;
import coords.InternalPoint;
import settings.GlobalContext;
import utility.GeometricCalculations;

/**
 * Wall is represented internally as a line segment. It can be associated with
 * WallSegmentObjects.
 * 
 * Note: Structurally it would be best to only keep a single set of
 * WallSegmentObject. However, the JAXB parser for XML comes into issues. So
 * explicit types are required.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "Wall")
@XmlAccessorType(XmlAccessType.FIELD)
public class Wall extends InternalSegmentObject {

	public static final int WALL_SEGMENT_OBJECT_DEFAULT_SIZE = 50;

	@XmlElementWrapper(name = "Windows")
	@XmlElement(name = "Window")
	private Set<Window> windows;

	@XmlElementWrapper(name = "Doors")
	@XmlElement(name = "Door")
	private Set<Door> doors;

	public Wall() {
		super();
		windows = new LinkedHashSet<>();
		doors = new LinkedHashSet<>();
	}

	public Wall(InternalPoint point1, InternalPoint point2) {
		super(point1, point2);
		this.windows = new LinkedHashSet<>();
		this.doors = new LinkedHashSet<>();
	}

	/**
	 * Gets the windows associated with this wall.
	 * 
	 * @return Windows
	 */
	public Window[] getWindows() {
		return windows.toArray(new Window[0]);
	}

	/**
	 * Adds a window.
	 * 
	 * @param window Window
	 */
	public void addWindow(Window window) {
		windows.add(window);
	}

	/**
	 * Removes a window.
	 * 
	 * @param window Window to remove
	 */
	public void removeWindow(Window window) {
		windows.remove(window);
	}

	/**
	 * Gets doors associated with this wall.
	 * 
	 * @return Door
	 */
	public Door[] getDoors() {
		return doors.toArray(new Door[0]);
	}

	/**
	 * Adds a door.
	 * 
	 * @param door Door
	 */
	public void addDoor(Door door) {
		doors.add(door);
	}

	/**
	 * Removes a door.
	 * 
	 * @param door Door to remove
	 */
	public void removeDoor(Door door) {
		doors.remove(door);
	}

	/**
	 * Gets all WallSegmentObjects associated with this wall, including windows and
	 * door.
	 * 
	 * @return WallSegmentObjects
	 */
	public WallSegmentObject[] getWallSegmentObjects() {
		Set<WallSegmentObject> wallSegmentObjects = new LinkedHashSet<>();
		wallSegmentObjects.addAll(windows);
		wallSegmentObjects.addAll(doors);

		return wallSegmentObjects.toArray(new WallSegmentObject[0]);
	}

	/**
	 * Removes a WallSegmentObject. This could be a window or a door.
	 * 
	 * @param wallSegmentObject WallSegmentObject
	 */
	public void removeWallSegmentObject(WallSegmentObject wallSegmentObject) {
		if (wallSegmentObject instanceof Window)
			windows.remove(wallSegmentObject);
		else if (wallSegmentObject instanceof Door)
			doors.remove(wallSegmentObject);
	}

	/**
	 * Draws a line based on specifications.
	 * 
	 * @param gc        Graphics object
	 * @param globals   GlobalContext
	 * @param color     Color of line
	 * @param lineWidth Width of line
	 */
	private void drawLineLayer(GC gc, GlobalContext globals, Color color, int lineWidth) {
		gc.setForeground(color);
		gc.setLineWidth((int) (lineWidth * globals.getZoomFactor()));

		DisplayPoint displayPoint1 = point1.getDisplayPoint(globals);
		DisplayPoint displayPoint2 = point2.getDisplayPoint(globals);

		gc.drawLine(displayPoint1.getX(), displayPoint1.getY(), displayPoint2.getX(), displayPoint2.getY());
	}

	/**
	 * Draws wall. If specified selected, wall is drawn green.
	 * 
	 * @param gc       Graphics object
	 * @param globals  GlobalContext
	 * @param selected If the wall should be drawn as selected or not
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		draw(gc, globals, selected, Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
	}

	/**
	 * Draws wall. If specified selected, wall is drawn as the specified color.
	 * 
	 * @param gc             Graphics object
	 * @param globals        GlobalContext
	 * @param selected       If the wall should be drawn as selected or not
	 * @param selectionColor Color to draw wall if specified as selected
	 */
	public void draw(GC gc, GlobalContext globals, boolean selected, Color selectionColor) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY), 10);
		drawLineLayer(gc, globals, Display.getCurrent().getSystemColor(SWT.COLOR_GRAY), 4);

		// draw transparent overlay to denote selection
		if (selected) {
			try {
				gc.setAlpha(128);
			} catch (SWTException ex) {
			}

			drawLineLayer(gc, globals, selectionColor, 10);
		}
	}

	/**
	 * Draw measurement label in center of wall.
	 * 
	 * @param gc      Graphics object
	 * @param globals GlobalContext
	 */
	public void drawMeasurementLabel(GC gc, GlobalContext globals) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
		gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// TODO: add font as a global value, to be disposed at end of application rather
		// than on every paint
		Font font = new Font(Display.getCurrent(), "Helvetica", 12, SWT.BOLD);
		gc.setFont(font);

		InternalPoint internalCenter = new InternalPoint((point1.getX() + point2.getX()) / 2,
				(point1.getY() + point2.getY()) / 2);
		DisplayPoint centerDisplayPoint = internalCenter.getDisplayPoint(globals);

		String displayString = GeometricCalculations.getDistanceString(point1, point2, globals);

		int approxStrWidth = (int) Math.round(displayString.length() * gc.getFontMetrics().getAverageCharacterWidth());
		int approxStrHeight = gc.getFontMetrics().getHeight();

		gc.drawText(displayString, centerDisplayPoint.getX() - approxStrWidth / 2,
				centerDisplayPoint.getY() - approxStrHeight / 2);

		font.dispose();
	}

	/**
	 * Creates a deep clone of this wall.
	 * 
	 * @param includeWallSegmentObjects Whether to explicitly include
	 *                                  WallSegmentObjects or not
	 * @return Deep clone of this wall
	 */
	public Wall makeClone(boolean includeWallSegmentObjects) {
		Wall wall = new Wall(point1, point2);

		if (includeWallSegmentObjects) {
			for (Window window : windows)
				wall.addWindow(window.makeClone(wall));

			for (Door door : doors)
				wall.addDoor(door.makeClone(wall));
		}

		return wall;
	}
}
