package objects;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.graphics.Rectangle;

import coords.InternalPoint;
import utility.GeometricCalculations;

/**
 * InternalObjectContext holds all the applications internal objects as a single
 * container.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "Objects")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalObjectContext {

	@XmlElementWrapper(name = "Walls")
	@XmlElement(name = "Wall")
	private Set<Wall> walls;

	@XmlElementWrapper(name = "ImageObjects")
	@XmlElement(name = "ImageObject")
	private Set<ImageObject> imageObjects;

	public InternalObjectContext() {
		walls = new LinkedHashSet<>();
		imageObjects = new LinkedHashSet<>();
	}

	/**
	 * Gets all walls.
	 * 
	 * @return All walls
	 */
	public Wall[] getWalls() {
		return walls.toArray(new Wall[0]);
	}

	/**
	 * Adds a wall.
	 * 
	 * @param wall Wall
	 */
	public void addWall(Wall wall) {
		walls.add(wall);
	}

	/**
	 * Removes the specified wall.
	 * 
	 * @param wall Wall
	 */
	public void removeWall(Wall wall) {
		walls.remove(wall);
	}

	/**
	 * Gets all image objects.
	 * 
	 * @return All image objects
	 */
	public ImageObject[] getImageObjects() {
		return imageObjects.toArray(new ImageObject[0]);
	}

	/**
	 * Adds an ImageObject
	 * 
	 * @param imageObject ImageObject
	 */
	public void addImageObject(ImageObject imageObject) {
		imageObjects.add(imageObject);
	}

	/**
	 * Removes the specified ImageObject.
	 * 
	 * @param imageObject ImageObject to remove
	 */
	public void removeImageObject(ImageObject imageObject) {
		imageObjects.remove(imageObject);
	}

	/**
	 * Gets all internal objects of the current context.
	 * 
	 * @param includeWallSegmentObjects Whether to explicitly include
	 *                                  WallSegmentObjects or not
	 * @return All internal objects of the current context
	 */
	public InternalObject[] getAllObjects(boolean includeWallSegmentObjects) {
		Set<InternalObject> internalObjects = new LinkedHashSet<>();

		for (Wall wall : walls) {
			internalObjects.add(wall);

			if (includeWallSegmentObjects) {
				for (WallSegmentObject wallSegmentObject : wall.getWallSegmentObjects()) {
					internalObjects.add(wallSegmentObject);
				}
			}
		}

		for (ImageObject imageObject : imageObjects) {
			internalObjects.add(imageObject);
		}

		return internalObjects.toArray(new InternalObject[0]);
	}

	/**
	 * Adds a collection of InternalObjects.
	 * 
	 * @param internalObjects Collection of InternalObjects
	 */
	public void addObjects(Collection<? extends InternalObject> internalObjects) {
		if (internalObjects != null) {
			for (InternalObject internalObject : internalObjects) {
				if (internalObject instanceof Wall) {
					Wall wall = (Wall) internalObject;
					addWall(wall);
				} else if (internalObject instanceof ImageObject) {
					ImageObject imageObject = (ImageObject) internalObject;
					addImageObject(imageObject);
				}
			}
		}
	}

	/**
	 * Removes a collection of InternalObjects.
	 * 
	 * @param internalObjects Collection of InternalObjects to remove
	 */
	public void removeObjects(Collection<? extends InternalObject> internalObjects) {
		if (internalObjects != null) {
			for (InternalObject internalObject : internalObjects) {
				if (internalObject instanceof Wall) {
					Wall wall = (Wall) internalObject;
					removeWall(wall);
				} else if (internalObject instanceof WallSegmentObject) {
					WallSegmentObject wallSegmentObject = (WallSegmentObject) internalObject;
					for (Wall wall : getWalls()) {
						wall.removeWallSegmentObject(wallSegmentObject);
					}
				} else if (internalObject instanceof ImageObject) {
					ImageObject imageObject = (ImageObject) internalObject;
					removeImageObject(imageObject);
				}
			}
		}
	}

	/**
	 * Removes all associated objects.
	 */
	public void removeAll() {
		walls.clear();
		imageObjects.clear();
	}

	/**
	 * Removes all associated objects, then add all objects of specified context.
	 * 
	 * Note: Does a shallow copy, i.e., all objects within are not cloned
	 * themselves.
	 * 
	 * @param internalObjectContext InternalObjectContext
	 */
	public void replaceAll(InternalObjectContext internalObjectContext) {
		removeAll();
		addAll(internalObjectContext);
	}

	/**
	 * Adds all objects of the specified context.
	 * 
	 * Note: Does a shallow copy, i.e., all objects within are not cloned
	 * themselves.
	 * 
	 * @param internalObjectContext InternalObjectContext
	 */
	public void addAll(InternalObjectContext internalObjectContext) {
		addObjects(internalObjectContext.walls);
		addObjects(internalObjectContext.imageObjects);
	}

	/**
	 * Gets the closest wall to the specified point.
	 * 
	 * @param point InternalPoint
	 * @return Closest wall if one exists, otherwise null
	 */
	public Wall getClosestWallTo(InternalPoint point) {
		double bestDist = Double.MAX_VALUE;
		Wall closest = null;
		for (Wall wall : walls) {
			InternalPoint closestWallPointToMouse = GeometricCalculations.getClosestPointOnSegment(wall.getPoint1(),
					wall.getPoint2(), point);

			double curDist = point.getDistanceTo(closestWallPointToMouse);
			// do not associate any wall with window if distance is 25 or more
			if (curDist < 25 && curDist <= bestDist) {
				bestDist = curDist;
				closest = wall;
			}
		}

		return closest;
	}

	/**
	 * Gets the closest object to the specified point.
	 * 
	 * @param point InternalPoint
	 * @return Closest InternalObject to the specified point
	 */
	public InternalObject getClosestObjectTo(InternalPoint point) {
		double bestDist = Double.MAX_VALUE;
		InternalObject closest = null;
		for (InternalObject internalObject : getAllObjects(true)) {
			if (internalObject instanceof InternalSegmentObject) {
				InternalSegmentObject internalSegmentObject = (InternalSegmentObject) internalObject;
				InternalPoint closestObjectPointToMouse = GeometricCalculations.getClosestPointOnSegment(
						internalSegmentObject.getPoint1(), internalSegmentObject.getPoint2(), point);

				double curDist = point.getDistanceTo(closestObjectPointToMouse);
				// not close if distance is 25 or more
				if (curDist < 25 && curDist <= bestDist) {
					// if distance is same, give other objects priority over wall
					if (curDist == bestDist) {
						if (!(internalObject instanceof Wall)) {
							bestDist = curDist;
							closest = internalObject;
						}
					} else {
						bestDist = curDist;
						closest = internalObject;
					}
				}
			} else if (internalObject instanceof ImageObject) {
				ImageObject imageObject = (ImageObject) internalObject;
				Rectangle imageObjectBounds = imageObject.getInternalBounds();
				if (imageObjectBounds.contains(point.getX(), point.getY())) {
					bestDist = 0;
					closest = internalObject;
				}
			}
		}

		return closest;
	}

	/**
	 * Creates a copy of the current context.
	 * 
	 * Note: Does a shallow copy, i.e., all objects within are not cloned
	 * themselves.
	 * 
	 * @return Copy of the current context
	 */
	public InternalObjectContext makeClone() {
		InternalObjectContext newInternalObjectContext = new InternalObjectContext();
		newInternalObjectContext.addObjects(newInternalObjectContext.walls);
		newInternalObjectContext.addObjects(newInternalObjectContext.imageObjects);

		return newInternalObjectContext;
	}
}
