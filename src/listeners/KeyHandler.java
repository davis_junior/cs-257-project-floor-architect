package listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

import settings.GlobalContext;

/**
 * KeyHandler is a listener for all key events associated with the application.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class KeyHandler implements KeyListener {

	private GlobalContext globals;

	public KeyHandler(GlobalContext globals) {
		this.globals = globals;
	}

	/**
	 * Handles key press events.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.keyCode == SWT.CTRL)
			globals.setCtrlDown(true);

		if (((e.stateMask & SWT.CTRL) != SWT.CTRL)) {
			if (e.keyCode == SWT.DEL) {
				if (globals.getToolbarSettings().isSelectMode()) {
					globals.getObjects().removeObjects(globals.getSelectedObjects());

					globals.getClosestObjects().clear();
					globals.getSelectedObjects().clear();

					globals.getController().getCanvas().redraw();
				}
			}
		}
	}

	/**
	 * Handles key release events.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.keyCode == SWT.CTRL)
			globals.setCtrlDown(false);
	}

}
