package listeners;

import java.util.Collection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import coords.DisplayPoint;
import objects.Door;
import objects.ImageObject;
import objects.InternalObject;
import objects.Wall;
import objects.WallSegmentObject;
import objects.Window;
import settings.GlobalContext;

/**
 * PaintHandler is a listener for the paint event of the canvas. All
 * InternalObjects in the object context are drawn here as well as any current
 * build objects.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class PaintHandler implements PaintListener {

	private GlobalContext globals;

	public PaintHandler(GlobalContext globals) {
		this.globals = globals;
	}

	/**
	 * Draws a plain white background.
	 * 
	 * @param gc Graphics object
	 */
	private void drawBackground(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		Rectangle clientArea = globals.getController().getCanvas().getClientArea();

		gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(clientArea);
	}

	/**
	 * Draws grid based on zoom and current base point.
	 * 
	 * @param gc Graphics object
	 */
	private void drawGrid(GC gc) {
		try {
			gc.setAlpha(255);
			// anti-aliasing is an unnecessary performance impact here
			// turn off now, turn back on on return
			gc.setAntialias(SWT.OFF);
		} catch (SWTException ex) {
		}

		if (globals.getToolbarSettings().isShowGrid()) {
			Rectangle clientArea = globals.getController().getCanvas().getClientArea();

			gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
			gc.setLineWidth(1);

			DisplayPoint nearestBaseGridDisplayPoint = globals.getBasePoint().getNearestGridPoint(globals)
					.getDisplayPoint(globals);

			// draw vertical lines
			for (int xincr = nearestBaseGridDisplayPoint.getX(); xincr < clientArea.width; xincr += globals
					.getGridDisplayWidth()) {
				gc.drawLine(xincr, 0, xincr, clientArea.height);
			}

			// draw horizontal lines
			for (int yincr = nearestBaseGridDisplayPoint.getY(); yincr < clientArea.height; yincr += globals
					.getGridDisplayWidth()) {
				gc.drawLine(0, yincr, clientArea.width, yincr);
			}

		}

		try {
			gc.setAlpha(255);
			gc.setAntialias(SWT.ON);
		} catch (SWTException ex) {
		}
	}

	/**
	 * Draws all saved walls and any current build walls. The closest wall is
	 * denoted by a different color if applicable.
	 * 
	 * @param gc Graphics object
	 */
	private void drawWalls(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		for (Wall wall : globals.getObjects().getWalls()) {
			wall.draw(gc, globals, false);
		}

		// show closest wall during window or door build mode
		if (globals.getButtonSettings().isBuildWindowMode() || globals.getButtonSettings().isBuildDoorMode()) {
			globals.getBuildWallSegmentObjectClosestWall().draw(gc, globals, true,
					Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		}

		if (globals.isBuildWallActive()) {
			globals.getBuildWall().draw(gc, globals, true);
		}
	}

	/**
	 * Draws all selected objects with a different color to denote selection.
	 * 
	 * @param gc Graphics object
	 */
	private void drawSelectedObject(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		if (globals.getToolbarSettings().isSelectMode()) {
			drawObjects(gc, globals.getClosestObjects(), true, Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
			drawObjects(gc, globals.getSelectedObjects(), true);
		}
	}

	/**
	 * Draws all objects currently in the process of being moved. The color is
	 * different to denote selection.
	 * 
	 * @param gc Graphics object
	 */
	private void drawMoveObjects(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		if (globals.isMoveActive())
			drawObjects(gc, globals.getMoveInternalObjects(), true,
					Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
	}

	/**
	 * Draws InternalObjects in preferred order.
	 * 
	 * @param gc              Graphics object
	 * @param internalObjects InternalObjects
	 * @param selected        Selected
	 */
	private void drawObjects(GC gc, Collection<InternalObject> internalObjects, boolean selected) {
		drawObjects(gc, internalObjects, selected, Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
	}

	/**
	 * Draws InternalObjects in preferred order.
	 * 
	 * @param gc              Graphics object
	 * @param internalObjects InternalObjects
	 * @param selected        Selected
	 * @param color           Selection color
	 */
	private void drawObjects(GC gc, Collection<InternalObject> internalObjects, boolean selected, Color color) {
		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof ImageObject) {
				ImageObject imageObject = (ImageObject) internalObject;
				imageObject.draw(gc, globals, selected, color);
			}
		}

		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof Wall) {
				Wall wall = (Wall) internalObject;
				wall.draw(gc, globals, selected, color);
			}
		}

		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof Window) {
				Window window = (Window) internalObject;
				window.draw(gc, globals, selected, color);
			}
		}

		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof Door) {
				Door door = (Door) internalObject;
				door.draw(gc, globals, selected, color);
			}
		}
	}

	/**
	 * Draws all saved wall segments. Also draws current build wall segment if in
	 * build mode.
	 * 
	 * @param gc Graphics object
	 */
	private void drawWallSegmentObjects(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		for (Wall wall : globals.getObjects().getWalls()) {
			for (WallSegmentObject wallSegmentObject : wall.getWallSegmentObjects()) {
				if (wallSegmentObject instanceof Window) {
					Window window = (Window) wallSegmentObject;
					window.draw(gc, globals, false);
				} else if (wallSegmentObject instanceof Door) {
					Door door = (Door) wallSegmentObject;
					door.draw(gc, globals, false);
				}
			}
		}

		if (globals.isBuildWallSegmentObjectActive()) {
			WallSegmentObject wallSegmentObject = globals.getBuildWallSegmentObject();
			if (wallSegmentObject instanceof Window) {
				Window window = (Window) wallSegmentObject;
				window.draw(gc, globals, true);
			} else if (wallSegmentObject instanceof Door) {
				Door door = (Door) wallSegmentObject;
				door.draw(gc, globals, true);
			}
		}
	}

	/**
	 * Draws all image objects. Current build image objects are drawn in a different
	 * color if applicable.
	 * 
	 * @param gc Graphics object
	 */
	private void drawImageObjects(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		for (ImageObject imageObject : globals.getObjects().getImageObjects()) {
			imageObject.draw(gc, globals, false);
		}

		if (globals.isBuildImageObjectActive()) {
			ImageObject buildImageObject = globals.getBuildImageObject();
			if (buildImageObject != null) {
				buildImageObject.draw(gc, globals, true, Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
			}
		}
	}

	/**
	 * Draws measurement labels of all walls.
	 * 
	 * @param gc Graphics object
	 */
	private void drawMeasurementLabels(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		if (globals.getToolbarSettings().isShowMeasurements()) {
			for (Wall wall : globals.getObjects().getWalls()) {
				wall.drawMeasurementLabel(gc, globals);
			}
		}

		if (globals.isBuildWallActive()) {
			globals.getBuildWall().drawMeasurementLabel(gc, globals);
		}
	}

	/**
	 * Draws all selected objects if applicable with a different color to denote
	 * selection.
	 * 
	 * @param gc Graphics object
	 */
	private void drawGroupSelection(GC gc) {
		try {
			gc.setAlpha(255);
		} catch (SWTException ex) {
		}

		if (globals.isGroupSelectionActive()) {
			Rectangle groupSelectionRectangle = globals.getGroupSelectionDisplayBounds();
			gc.setLineWidth(2);

			try {
				gc.setAlpha(255);
			} catch (SWTException ex) {
			}
			gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
			gc.drawRectangle(groupSelectionRectangle);

			try {
				gc.setAlpha(15);
			} catch (SWTException ex) {
			}
			gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
			gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
			gc.fillRectangle(groupSelectionRectangle);
		}
	}

	/**
	 * Handles the main paint events of the canvas. All internal objects are drawn
	 * here, including the grid and selected objects.
	 */
	@Override
	public void paintControl(PaintEvent e) {
		try {
			// anti-aliasing for smoother lines if supported by operating system
			e.gc.setAntialias(SWT.ON);
			e.gc.setTextAntialias(SWT.ON);
		} catch (SWTException ex) {
		}

		drawBackground(e.gc);
		drawGrid(e.gc);
		drawImageObjects(e.gc);
		drawWalls(e.gc);
		drawWallSegmentObjects(e.gc);
		drawSelectedObject(e.gc);
		drawMoveObjects(e.gc);
		drawMeasurementLabels(e.gc);
		drawGroupSelection(e.gc);
	}
}
