package listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolItem;

import components.AboutBox;
import coords.InternalPoint;
import objects.ImageObject;
import objects.Wall;
import objects.Window;
import settings.GlobalContext;

/**
 * ButtonHandler is a listener for all button types, including menu buttons,
 * tool bar buttons, and object library buttons. Button events are handled here.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ButtonHandler implements SelectionListener {

	private GlobalContext globals;

	public ButtonHandler(GlobalContext globals) {
		this.globals = globals;
	}

	/**
	 * This method currently has no particular use.
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		globals.getController().getCanvas().redraw();
	}

	/**
	 * Handles tool item selection events.
	 * 
	 * @param toolItem ToolItem
	 */
	private void handleToolItemSelect(ToolItem toolItem) {
		String command = (String) toolItem.getData("command");
		if (command != null) {
			switch (command) {

			case "new":
				newFile();
				break;

			case "open":
				openFile();
				break;

			case "save":
				saveFile();
				break;

			case "pan_mode":
				globals.getButtonSettings().deselectAll();
				break;

			case "select_mode":
				globals.getButtonSettings().deselectAll();
				break;

			case "snap_to_grid":
				break;

			case "show_grid":
				break;

			case "show_measurements":
				break;

			case "zoom_in":
				zoomIn();
				break;

			case "zoom_out":
				zoomOut();
				break;
			}
		}
	}

	/**
	 * Handles button selection events.
	 * 
	 * @param button Button
	 */
	private void handleButtonSelect(Button button) {
		String command = (String) button.getData("command");
		if (command != null) {
			switch (command) {
			case "wall_build_mode":
				break;

			case "window_build_mode":
				break;

			case "door_build_mode":
				break;

			}
		}

		if (command.endsWith("_image_build_mode")) {
			String name = (String) button.getData("name");
			String group = (String) button.getData("group");
			String path = (String) button.getData("path");

			ImageObject imageObject = new ImageObject(globals, name, path, new InternalPoint());
			globals.setBuildImageObject(imageObject);
			globals.setBuildImageObjectActive(true);
		}
	}

	/**
	 * Handles menu item selection events.
	 * 
	 * @param menuItem MenuItem
	 */
	private void handleMenuItemSelect(MenuItem menuItem) {
		String command = (String) menuItem.getData("command");
		if (command != null) {
			switch (command) {

			case "new":
				newFile();
				break;

			case "open_file":
				openFile();
				break;

			case "save":
				saveFile();
				break;

			case "save_as":
				saveFileAs();
				break;

			case "zoom_in":
				zoomIn();
				break;

			case "zoom_out":
				zoomOut();
				break;

			case "quit":
				if (globals.getController().promptQuit())
					return;

				break;

			case "about":
				AboutBox aboutBox = new AboutBox(globals.getController().getMainShell(), SWT.None);
				aboutBox.open();
				break;

			}
		}
	}

	/**
	 * Prompts for save then sets current project to blank project.
	 */
	private void newFile() {
		boolean doit = false;
		// skip prompt if empty file
		if (globals.getObjects().getAllObjects(true).length == 0 && globals.getFileContext().getFile() == null) {
			doit = true;
		} else {
			int result = globals.getFileContext().showSaveFileMessageBox("Create New File");
			if (result != SWT.CANCEL) {
				if (result == SWT.YES) {
					if (!globals.getFileContext().save())
						return;
				}

				doit = true;
			}
		}

		if (doit) {
			globals.getObjects().removeAll();
			globals.getFileContext().setFile(null);
			globals.getController().resetTitle(null);

			// reset default button settings
			globals.restoreDefaults();
		}
	}

	/**
	 * Prompts for save then opens a file dialog, loading the chosen file as current
	 * project.
	 */
	private void openFile() {
		boolean doit = false;
		// skip prompt if empty file
		if (globals.getObjects().getAllObjects(true).length == 0 && globals.getFileContext().getFile() == null) {
			doit = true;
		} else {
			int result = globals.getFileContext().showSaveFileMessageBox("Open File");
			if (result != SWT.CANCEL) {
				if (result == SWT.YES) {
					if (!globals.getFileContext().save())
						return;
				}

				doit = true;
			}
		}

		if (doit) {
			if (globals.getFileContext().open()) {
				// reset default button settings
				globals.restoreDefaults();

				// build image cache
				for (ImageObject imageObject : globals.getObjects().getImageObjects()) {
					globals.getImageContext().addImage(globals, imageObject.getPath());
				}
				globals.getImageContext().updateImages(globals);

				globals.getController().resetTitle(globals.getFileContext().getFile().getName());

			}
		}
	}

	/**
	 * Saves project to current file, opens file dialog if there is no associated
	 * file.
	 */
	private void saveFile() {
		if (globals.getFileContext().save())
			globals.getController().resetTitle(globals.getFileContext().getFile().getName());
	}

	/**
	 * Opens a file dialog and saves project to selected file.
	 */
	private void saveFileAs() {
		if (globals.getFileContext().saveAs())
			globals.getController().resetTitle(globals.getFileContext().getFile().getName());
	}

	/**
	 * Zooms in on canvas incrementally based on current zoom factor.
	 */
	private void zoomIn() {
		globals.setZoomFactor(globals.getZoomFactor() * 1.25);

		if (globals.getZoomFactor() > 4)
			globals.setZoomFactor(4);

		globals.getImageContext().updateImages(globals);
	}

	/**
	 * Zooms out on canvas incrementally based on current zoom factor.
	 */
	private void zoomOut() {
		globals.setZoomFactor(globals.getZoomFactor() * 0.75);

		if (globals.getZoomFactor() < 0.25)
			globals.setZoomFactor(0.25);

		globals.getImageContext().updateImages(globals);
	}

	/**
	 * Handle all selection events of components associated with this listener.
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		if (e.getSource() instanceof ToolItem) {
			ToolItem toolItem = (ToolItem) e.getSource();
			handleToolItemSelect(toolItem);
		} else if (e.getSource() instanceof MenuItem) {
			MenuItem menuItem = (MenuItem) e.getSource();
			handleMenuItemSelect(menuItem);

			if (globals.getController().getMainShell().isDisposed())
				return;
		} else if (e.getSource() instanceof Button) {
			Button button = (Button) e.getSource();

			if (button.getSelection()) {
				// deselect all other buttons
				globals.getButtonSettings().selectButtonOnly(button);
				handleButtonSelect(button);
			}
		}

		// cancel build wall if mode off
		if (!globals.getButtonSettings().isBuildWallMode()) {
			globals.setBuildWallActive(false);
			globals.setBuildWall(new Wall());
		}

		// cancel build wall segment if mode off
		if (!globals.getButtonSettings().isBuildWindowMode() && !globals.getButtonSettings().isBuildDoorMode()) {
			globals.setBuildWallSegmentObjectActive(false);
			globals.setBuildWallSegmentObject(new Window(new Wall()));
			globals.setBuildWallSegmentObjectAssociatedWall(new Wall());
			globals.setBuildWallSegmentObjectClosestWall(new Wall());
		}

		// cancel build image object if mode off
		if (!globals.getButtonSettings().isImageObjectBuildMode()) {
			globals.setBuildImageObject(null);
			globals.setBuildImageObjectActive(false);
		}

		// cancel pan if mode off
		if (!globals.getToolbarSettings().isPanMode() || globals.getButtonSettings().isAnyObjectBuildMode())
			globals.setPanActive(false);

		// reset selection if selection mode off
		if (!globals.getToolbarSettings().isSelectMode() || globals.getButtonSettings().isAnyObjectBuildMode()) {
			globals.getClosestObjects().clear();
			globals.getSelectedObjects().clear();
		}

		// clear closest objects if selection mode
		if (globals.getToolbarSettings().isSelectMode())
			globals.getClosestObjects().clear();

		globals.getController().getCanvas().redraw();
	}
}
