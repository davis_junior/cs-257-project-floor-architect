package listeners;

import java.util.Collection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import components.ContextMenu;
import coords.DisplayPoint;
import coords.InternalPoint;
import objects.Door;
import objects.ImageObject;
import objects.InternalObject;
import objects.InternalSegmentObject;
import objects.Wall;
import objects.WallSegmentObject;
import objects.Window;
import settings.GlobalContext;
import utility.GeometricCalculations;

/**
 * MouseHandler is a listener for all mouse events for the canvas.
 * 
 * TODO: fix group selection - deselecting a wall segment with wall still
 * selected disassociates wall segment from wall
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class MouseHandler implements MouseMoveListener, MouseListener, MouseTrackListener {

	private static final int MOUSE_LEFT_BUTTON = 1;
	private static final int MOUSE_MIDDLE_BUTTON = 2;
	private static final int MOUSE_RIGHT_BUTTON = 3;

	private GlobalContext globals;

	private Cursor cursor;
	private int mouseStyle;

	// wall creation
	private DisplayPoint buildWallMouseStart;
	private DisplayPoint buildWallMouseEnd;

	// pan and zoom
	private DisplayPoint panMouseStart;
	private InternalPoint panBaseStart;

	public MouseHandler(GlobalContext globals) {
		this.globals = globals;

		buildWallMouseStart = new DisplayPoint();
		buildWallMouseEnd = new DisplayPoint();

		panMouseStart = new DisplayPoint();
		panBaseStart = new InternalPoint();

		cursor = globals.getController().getMainShell().getCursor();
		mouseStyle = -1;
		changeCursor(SWT.CURSOR_ARROW);
	}

	/**
	 * Sets current build wall end coordinates to mouse.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_buildWall(DisplayPoint mouse) {
		if (globals.getToolbarSettings().isSnapToGrid()) {
			globals.getBuildWall()
					.setPoint1(buildWallMouseStart.getInternalPoint(globals).getNearestGridPoint(globals));
			globals.getBuildWall().setPoint2(mouse.getInternalPoint(globals).getNearestGridPoint(globals));
		} else {
			globals.getBuildWall().setPoint1(buildWallMouseStart.getInternalPoint(globals));
			globals.getBuildWall().setPoint2(mouse.getInternalPoint(globals));
		}
	}

	/**
	 * Finds and sets the global closest wall to the mouse for wall segment
	 * creation.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_saveClosestWall(DisplayPoint mouse) {
		Wall closest = globals.getObjects().getClosestWallTo(mouse.getInternalPoint(globals));
		if (closest != null) {
			globals.setBuildWallSegmentObjectClosestWall(closest);
			changeCursor(SWT.CURSOR_HAND);
		} else {
			globals.setBuildWallSegmentObjectClosestWall(new Wall());
			changeCursor(SWT.CURSOR_ARROW);
		}
	}

	/**
	 * Finds and sets the closest internal object to the mouse.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_saveClosestObject(DisplayPoint mouse) {
		InternalObject closest = globals.getObjects().getClosestObjectTo(mouse.getInternalPoint(globals));
		if (closest != null) {
			globals.getClosestObjects().clear();
			globals.getClosestObjects().add(closest);

			// add all wall objects if wall
			if (closest instanceof Wall) {
				Wall wall = (Wall) closest;
				for (WallSegmentObject wallSegmentObject : wall.getWallSegmentObjects()) {
					if (!globals.getClosestObjects().contains(wallSegmentObject))
						globals.getClosestObjects().add(wallSegmentObject);
				}
			}
		} else {
			globals.getClosestObjects().clear();
		}
	}

	/**
	 * Finds and sets selected objects to those within the current mouse group
	 * selection box.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_saveGroupSelectionClosestObjects(DisplayPoint mouse) {
		globals.getClosestObjects().clear();

		Rectangle groupSelectionInternalBounds = globals.getGroupSelectionInternalBounds(globals);

		for (InternalObject internalObject : globals.getObjects().getAllObjects(true)) {
			if (internalObject instanceof InternalSegmentObject) {
				InternalSegmentObject internalSegmentObject = (InternalSegmentObject) internalObject;

				if (GeometricCalculations.intersects(groupSelectionInternalBounds, internalSegmentObject.getPoint1(),
						internalSegmentObject.getPoint2())) {
					globals.getClosestObjects().add(internalObject);

					// add all wall objects if wall
					if (internalObject instanceof Wall) {
						Wall wall = (Wall) internalObject;
						for (WallSegmentObject wallSegmentObject : wall.getWallSegmentObjects()) {
							if (!globals.getClosestObjects().contains(wallSegmentObject))
								globals.getClosestObjects().add(wallSegmentObject);
						}
					}
				}
			} else if (internalObject instanceof ImageObject) {
				ImageObject imageObject = (ImageObject) internalObject;
				if (groupSelectionInternalBounds.intersects(imageObject.getInternalBounds())) {
					globals.getClosestObjects().add(imageObject);
				}
			}
		}
	}

	/**
	 * Moves WallSegmentObject based on mouse and its associated wall. Angle and
	 * end-points of associated wall are taken into account.
	 * 
	 * @param mouse             Mouse coordinates
	 * @param wallSegmentObject WallSegmentObject
	 * @param associatedWall    Associated Wall
	 */
	private void moveWallSegment(DisplayPoint mouse, WallSegmentObject wallSegmentObject, Wall associatedWall) {
		InternalPoint mouseInternalPoint = mouse.getInternalPoint(globals);

		double angle = associatedWall.getAngle();

		InternalPoint point1 = GeometricCalculations.getClosestPointOnSegment(associatedWall.getPoint1(),
				associatedWall.getPoint2(), mouseInternalPoint);
		InternalPoint point2 = new InternalPoint(
				point1.getX() + (int) Math.round(Wall.WALL_SEGMENT_OBJECT_DEFAULT_SIZE * Math.cos(angle)),
				point1.getY() + (int) Math.round(Wall.WALL_SEGMENT_OBJECT_DEFAULT_SIZE * Math.sin(angle)));

		if (point1.getDistanceTo(associatedWall.getPoint2()) < Wall.WALL_SEGMENT_OBJECT_DEFAULT_SIZE) {
			point2 = new InternalPoint(associatedWall.getPoint2());
			point1 = new InternalPoint(
					point2.getX() - (int) Math.round(Wall.WALL_SEGMENT_OBJECT_DEFAULT_SIZE * Math.cos(angle)),
					point2.getY() - (int) Math.round(Wall.WALL_SEGMENT_OBJECT_DEFAULT_SIZE * Math.sin(angle)));
		}

		if (globals.getToolbarSettings().isSnapToGrid() && associatedWall.isStraight()) {
			wallSegmentObject.setPoint1(point1.getNearestGridPoint(globals));
			wallSegmentObject.setPoint2(point2.getNearestGridPoint(globals));
		} else {
			wallSegmentObject.setPoint1(point1);
			wallSegmentObject.setPoint2(point2);
		}
	}

	/**
	 * Moves wall segment based on position of mouse.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_buildWallSegmentObject(DisplayPoint mouse) {
		moveWallSegment(mouse, globals.getBuildWallSegmentObject(), globals.getBuildWallSegmentObjectAssociatedWall());
	}

	/**
	 * Moves currently built image object on mouse move.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_buildImageObject(DisplayPoint mouse) {
		InternalPoint mouseInternalPoint = mouse.getInternalPoint(globals);

		if (globals.getBuildImageObject() != null) {
			if (globals.getToolbarSettings().isSnapToGrid())
				globals.getBuildImageObject().setPoint(mouseInternalPoint.getNearestGridPoint(globals));
			else
				globals.getBuildImageObject().setPoint(mouseInternalPoint);
		}
	}

	/**
	 * Pans screen by shifting base point based on mouse move.
	 * 
	 * @param mouse Mouse coordinates
	 */
	private void mouseMove_pan(DisplayPoint mouse) {
		int baseX = (int) ((panBaseStart.getX()
				+ (panMouseStart.getX() - mouse.getX()) * globals.getInverseZoomFactor()));
		int baseY = (int) ((panBaseStart.getY()
				+ (panMouseStart.getY() - mouse.getY()) * globals.getInverseZoomFactor()));
		globals.setBasePoint(new InternalPoint(baseX, baseY));
	}

	/**
	 * Handles mouse move events.
	 */
	@Override
	public void mouseMove(MouseEvent e) {
		DisplayPoint mouse = new DisplayPoint(e.x, e.y);

		// event button field always contains 0, use these instead
		boolean leftButtonDown = (e.stateMask & SWT.BUTTON1) != 0;
		boolean middleButtonDown = (e.stateMask & SWT.BUTTON2) != 0;
		boolean rightButtonDown = (e.stateMask & SWT.BUTTON3) != 0;

		InternalPoint mouseInternalPoint = mouse.getInternalPoint(globals);
		globals.getController().getLabelCoords()
				.setText("Coords: " + mouseInternalPoint.getX() + ", " + mouseInternalPoint.getY());

		if (globals.getButtonSettings().isAnyObjectBuildMode()) {
			if (globals.isBuildWallActive()) {
				mouseMove_buildWall(mouse);
			} else if (globals.getButtonSettings().isBuildWindowMode()
					|| globals.getButtonSettings().isBuildDoorMode()) {
				if (globals.isBuildWallSegmentObjectActive())
					mouseMove_buildWallSegmentObject(mouse);
				else
					mouseMove_saveClosestWall(mouse);
			} else if (globals.isBuildImageObjectActive()) {
				mouseMove_buildImageObject(mouse);
			}
		} else if (globals.isPanActive()) {
			mouseMove_pan(mouse);
		} else if (globals.getToolbarSettings().isSelectMode()) {
			if (globals.isMoveActive()) {
				globals.setMoveMouseEnd(mouse);

				globals.setMoveInternalObjects(InternalObject.cloneAll(globals, globals.getSelectedObjects()));
				moveObjectsOnMouse(globals.getMoveInternalObjects());

			} else {
				mouseMove_saveClosestObject(mouse);

				boolean hoveringSelected = false;
				if (globals.getSelectedObjects().size() > 0 && globals.getClosestObjects().size() > 0) {
					if (globals.getSelectedObjects().containsAll(globals.getClosestObjects())) {
						hoveringSelected = true;
						if (!globals.isCtrlDown())
							changeCursor(SWT.CURSOR_SIZEALL);
						else
							changeCursor(SWT.CURSOR_HAND);
					}
				}

				if (!hoveringSelected) {
					if (globals.getClosestObjects().isEmpty())
						changeCursor(SWT.CURSOR_ARROW);
					else
						changeCursor(SWT.CURSOR_HAND);
				}

				if (globals.isGroupSelectionActive()) {
					if (leftButtonDown) {
						globals.setGroupSelectionMouseEnd(new DisplayPoint(mouse));

						Rectangle groupSelectionRect = globals.getGroupSelectionDisplayBounds();
						int groupSelectionArea = Math.abs(groupSelectionRect.width * groupSelectionRect.height);
						if (groupSelectionArea > 0) {
							changeCursor(SWT.CURSOR_ARROW);
							mouseMove_saveGroupSelectionClosestObjects(mouse);
						}
					}
				}
			}
		}

		globals.getController().getCanvas().redraw();
	}

	/**
	 * Handles mouse double click events.
	 */
	@Override
	public void mouseDoubleClick(MouseEvent e) {
		DisplayPoint mouse = new DisplayPoint(e.x, e.y);

		globals.getController().getCanvas().redraw();
	}

	/**
	 * Starts wall build on first click, saves on second click.
	 * 
	 * @param mouse       Mouse coordinates
	 * @param mouseButton Mouse button
	 */
	private void mouseDown_buildWall(DisplayPoint mouse, int mouseButton) {
		if (mouseButton == MOUSE_LEFT_BUTTON) {
			// handle wall creation
			globals.setBuildWallActive(!globals.isBuildWallActive());

			if (globals.isBuildWallActive()) { // first mouse press
				globals.getController().getLabelMessage().setText("Left click to save wall. Right click to cancel.");

				buildWallMouseStart = new DisplayPoint(mouse);
				globals.setBuildWall(new Wall());
			} else { // second mouse press
				globals.getController().getLabelMessage()
						.setText("Left click to begin building a wall. Right click to cancel.");

				buildWallMouseEnd = new DisplayPoint(mouse);

				// save wall
				Wall saveWall;
				if (globals.getToolbarSettings().isSnapToGrid())
					saveWall = new Wall(buildWallMouseStart.getInternalPoint(globals).getNearestGridPoint(globals),
							buildWallMouseEnd.getInternalPoint(globals).getNearestGridPoint(globals));
				else
					saveWall = new Wall(buildWallMouseStart.getInternalPoint(globals),
							buildWallMouseEnd.getInternalPoint(globals));

				globals.getObjects().addWall(saveWall);
				globals.setBuildWall(new Wall());
			}
		} else if (mouseButton == MOUSE_RIGHT_BUTTON) {
			if (globals.isBuildWallActive()) {
				globals.getController().getLabelMessage()
						.setText("Left click to begin building a wall. Right click to cancel.");
			} else {
				globals.getButtonSettings().setBuildWallMode(false);
				setLabelMessageAndCursor();
			}

			globals.setBuildWallActive(false);
			globals.setBuildWall(new Wall());
		}
	}

	/**
	 * Saves the currently built wall segment on left click. Cancels process on
	 * right click.
	 * 
	 * @param mouse       Mouse coordinates
	 * @param mouseButton Mouse button
	 */
	private void mouseDown_buildWallSegmentObject(DisplayPoint mouse, int mouseButton) {
		if (mouseButton == MOUSE_LEFT_BUTTON) {
			// handle window creation
			globals.setBuildWallSegmentObjectActive(!globals.isBuildWallSegmentObjectActive());

			if (globals.isBuildWallSegmentObjectActive()) { // first mouse press
				// find closest wall
				Wall closest = globals.getObjects().getClosestWallTo(mouse.getInternalPoint(globals));
				if (closest != null) {
					if (globals.getButtonSettings().isBuildWindowMode()) {
						globals.getController().getLabelMessage()
								.setText("Left click to save window. Right click to cancel.");
						globals.setBuildWallSegmentObject(new Window(closest));
					} else if (globals.getButtonSettings().isBuildDoorMode()) {
						globals.getController().getLabelMessage()
								.setText("Left click to save door. Right click to cancel.");
						globals.setBuildWallSegmentObject(new Door(closest));
					}

					globals.setBuildWallSegmentObjectAssociatedWall(closest);
					globals.setBuildWallSegmentObjectClosestWall(closest);
					mouseMove_buildWallSegmentObject(mouse);
					changeCursor(SWT.CURSOR_HAND);
				} else {
					globals.setBuildWallSegmentObjectActive(false);
					globals.setBuildWallSegmentObject(new Window(new Wall()));
					globals.setBuildWallSegmentObjectAssociatedWall(new Wall());
					globals.setBuildWallSegmentObjectClosestWall(new Wall());
					changeCursor(SWT.CURSOR_ARROW);
				}
			} else { // second mouse press
				if (globals.getButtonSettings().isBuildWindowMode())
					globals.getController().getLabelMessage()
							.setText("Left click near a wall to begin building a window. Right click to cancel.");
				else if (globals.getButtonSettings().isBuildDoorMode())
					globals.getController().getLabelMessage()
							.setText("Left click near a wall to begin building a door. Right click to cancel.");

				if (globals.getButtonSettings().isBuildWindowMode()) {
					// save window
					Window saveWindow;
					if (globals.getToolbarSettings().isSnapToGrid()
							&& globals.getBuildWallSegmentObjectAssociatedWall().isStraight())
						saveWindow = new Window(globals.getBuildWallSegmentObjectAssociatedWall(),
								globals.getBuildWallSegmentObject().getPoint1().getNearestGridPoint(globals),
								globals.getBuildWallSegmentObject().getPoint2().getNearestGridPoint(globals));
					else
						saveWindow = new Window(globals.getBuildWallSegmentObjectAssociatedWall(),
								globals.getBuildWallSegmentObject().getPoint1(),
								globals.getBuildWallSegmentObject().getPoint2());

					globals.getBuildWallSegmentObjectAssociatedWall().addWindow(saveWindow);
				} else if (globals.getButtonSettings().isBuildDoorMode()) {
					// save door
					Door saveDoor;
					if (globals.getToolbarSettings().isSnapToGrid()
							&& globals.getBuildWallSegmentObjectAssociatedWall().isStraight())
						saveDoor = new Door(globals.getBuildWallSegmentObjectAssociatedWall(),
								globals.getBuildWallSegmentObject().getPoint1().getNearestGridPoint(globals),
								globals.getBuildWallSegmentObject().getPoint2().getNearestGridPoint(globals));
					else
						saveDoor = new Door(globals.getBuildWallSegmentObjectAssociatedWall(),
								globals.getBuildWallSegmentObject().getPoint1(),
								globals.getBuildWallSegmentObject().getPoint2());

					globals.getBuildWallSegmentObjectAssociatedWall().addDoor(saveDoor);
				}

				globals.setBuildWallSegmentObject(new Window(new Wall()));
				globals.setBuildWallSegmentObjectAssociatedWall(new Wall());
				globals.setBuildWallSegmentObjectClosestWall(new Wall());

				changeCursor(SWT.CURSOR_ARROW);
			}
		} else if (mouseButton == MOUSE_RIGHT_BUTTON) {
			if (!globals.isBuildWallSegmentObjectActive())
				globals.getButtonSettings().deselectAll();

			globals.setBuildWallSegmentObjectActive(false);
			globals.setBuildWallSegmentObject(new Window(new Wall()));
			globals.setBuildWallSegmentObjectAssociatedWall(new Wall());
			globals.setBuildWallSegmentObjectClosestWall(new Wall());

			setLabelMessageAndCursor();
		}
	}

	/**
	 * Saves the currently built image object on left click, cancels the process on
	 * right click.
	 * 
	 * @param mouse       Mouse coordinates
	 * @param mouseButton Mouse button
	 */
	private void mouseDown_buildImageObject(DisplayPoint mouse, int mouseButton) {
		InternalPoint mouseInternalPoint = mouse.getInternalPoint(globals);

		if (mouseButton == MOUSE_LEFT_BUTTON) {
			// handle image object creation
			if (globals.isBuildImageObjectActive()) {
				if (globals.getBuildImageObject() != null) {
					// save image object
					ImageObject saveImageObject = globals.getBuildImageObject().makeClone(globals);
					if (globals.getBuildImageObject() != null) {
						if (globals.getToolbarSettings().isSnapToGrid())
							saveImageObject.setPoint(mouseInternalPoint.getNearestGridPoint(globals));
						else
							saveImageObject.setPoint(mouseInternalPoint);

						globals.getObjects().addImageObject(saveImageObject);
					}
				}
			}
		} else if (mouseButton == MOUSE_RIGHT_BUTTON) {
			globals.getButtonSettings().deselectAll();

			globals.setBuildImageObjectActive(false);
			globals.setBuildImageObject(null);

			setLabelMessageAndCursor();
		}
	}

	/**
	 * Starts pan process on left press.
	 * 
	 * @param mouse       Mouse coordinates
	 * @param mouseButton Mouse button
	 */
	private void mouseDown_pan(DisplayPoint mouse, int mouseButton) {
		if (mouseButton == MOUSE_LEFT_BUTTON) {
			globals.setPanActive(true);
			panMouseStart = new DisplayPoint(mouse);
			panBaseStart = new InternalPoint(globals.getBasePoint());
		}
	}

	/**
	 * Handles mouse down events.
	 */
	@Override
	public void mouseDown(MouseEvent e) {
		DisplayPoint mouse = new DisplayPoint(e.x, e.y);

		if (globals.getButtonSettings().isAnyObjectBuildMode()) {
			if (globals.getButtonSettings().isBuildWallMode())
				mouseDown_buildWall(mouse, e.button);
			else if (globals.getButtonSettings().isBuildWindowMode() || globals.getButtonSettings().isBuildDoorMode())
				mouseDown_buildWallSegmentObject(mouse, e.button);
			else if (globals.getButtonSettings().isImageObjectBuildMode())
				mouseDown_buildImageObject(mouse, e.button);
		} else if (globals.getToolbarSettings().isPanMode()) {
			mouseDown_pan(mouse, e.button);
		} else if (globals.getToolbarSettings().isSelectMode()) {
			mouseMove_saveClosestObject(mouse);

			boolean hoveringSelected = false;
			if (globals.getSelectedObjects().size() > 0 && globals.getClosestObjects().size() > 0) {
				if (globals.getSelectedObjects().containsAll(globals.getClosestObjects())) {
					hoveringSelected = true;
					if (!globals.isCtrlDown()) {
						changeCursor(SWT.CURSOR_SIZEALL);
						globals.setMoveActive(true);
					} else
						changeCursor(SWT.CURSOR_HAND);
				}
			}

			if (!hoveringSelected) {
				if (globals.getClosestObjects().isEmpty())
					changeCursor(SWT.CURSOR_ARROW);
				else
					changeCursor(SWT.CURSOR_HAND);
			}

			if (e.button == MOUSE_LEFT_BUTTON) {
				if (globals.isMoveActive()) {
					globals.setMoveMouseStart(mouse);
					globals.setMoveInternalObjects(InternalObject.cloneAll(globals, globals.getSelectedObjects()));
					globals.getClosestObjects().clear();
				} else {
					globals.setGroupSelectionActive(true);
					globals.setGroupSelectionMouseStart(new DisplayPoint(mouse));
					globals.setGroupSelectionMouseEnd(new DisplayPoint(mouse));

					if (!globals.isCtrlDown())
						globals.getSelectedObjects().clear();
				}
			}

			if (e.button == MOUSE_RIGHT_BUTTON) {
				// cancel move mode
				globals.setMoveActive(false);
				globals.setMoveMouseStart(new DisplayPoint());
				globals.setMoveMouseEnd(new DisplayPoint());

				// if nothing selected, select closest
				if (globals.getSelectedObjects().isEmpty())
					globals.getSelectedObjects().addAll(globals.getClosestObjects());

				if (!globals.getSelectedObjects().isEmpty()) {
					globals.getClosestObjects().clear();

					ContextMenu contextMenu = new ContextMenu(globals.getSelectedObjects(), globals);
					contextMenu.open();
				}
			}
		}

		globals.getController().getCanvas().redraw();
	}

	/**
	 * Handles mouse up events.
	 */
	@Override
	public void mouseUp(MouseEvent e) {
		DisplayPoint mouse = new DisplayPoint(e.x, e.y);

		if (globals.getButtonSettings().isAnyObjectBuildMode()) {

		} else if (globals.getToolbarSettings().isPanMode()) {
			if (e.button == MOUSE_LEFT_BUTTON) {
				globals.setPanActive(false);
			}
		} else if (globals.getToolbarSettings().isSelectMode()) {
			if (e.button == MOUSE_LEFT_BUTTON) {
				if (globals.isMoveActive()) {
					globals.setMoveActive(false);
					globals.setMoveMouseEnd(mouse);

					moveObjectsOnMouse(globals.getSelectedObjects());

					// clear
					globals.setMoveMouseStart(new DisplayPoint());
					globals.setMoveMouseEnd(new DisplayPoint());
					globals.getMoveInternalObjects().clear();
				} else {
					globals.setGroupSelectionActive(false);
					globals.setGroupSelectionMouseEnd(new DisplayPoint(mouse));

					boolean remove = false;
					if (globals.isCtrlDown()) {
						Rectangle groupSelectionRectangle = globals.getGroupSelectionDisplayBounds();
						int area = groupSelectionRectangle.width * groupSelectionRectangle.height;
						if (area == 0) {
							if (globals.getSelectedObjects().containsAll(globals.getClosestObjects())) {
								remove = true;
								globals.getSelectedObjects().removeAll(globals.getClosestObjects());
							}
						}
					} else
						globals.getSelectedObjects().clear();

					if (!remove)
						globals.getSelectedObjects().addAll(globals.getClosestObjects());

					// reset
					globals.setGroupSelectionMouseStart(new DisplayPoint());
					globals.setGroupSelectionMouseEnd(new DisplayPoint());
					globals.getClosestObjects().clear();
				}
			}
		}

		globals.getController().getCanvas().redraw();
	}

	/**
	 * Handles mouse enter inside the canvas events.
	 */
	@Override
	public void mouseEnter(MouseEvent e) {
		setLabelMessageAndCursor();

		// globals.getCanvas().redraw();
	}

	/**
	 * Handles mouse exit from the canvas events.
	 */
	@Override
	public void mouseExit(MouseEvent e) {
		globals.getController().getLabelCoords().setText("Coords:");

		changeCursor(SWT.CURSOR_ARROW);
		globals.getController().getLabelMessage().setText("");

		if (globals.getButtonSettings().isAnyObjectBuildMode()) {
			if (!globals.isBuildWallSegmentObjectActive())
				globals.setBuildWallSegmentObjectClosestWall(new Wall());
		} else {
			if (globals.getToolbarSettings().isSelectMode())
				globals.getClosestObjects().clear();
		}

		globals.getController().getCanvas().redraw();
	}

	/**
	 * Handles mouse hover events.
	 */
	@Override
	public void mouseHover(MouseEvent e) {
		// globals.getCanvas().redraw();
	}

	/**
	 * Moves the specified InternalObjects based on mouse.
	 * 
	 * @param internalObjects InternalObjects
	 */
	private void moveObjectsOnMouse(Collection<InternalObject> internalObjects) {
		InternalPoint internalMouseStart = globals.getMoveMouseStart().getInternalPoint(globals);
		InternalPoint internalMouseEnd = globals.getMoveMouseEnd().getInternalPoint(globals);

		int xdelta = internalMouseEnd.getX() - internalMouseStart.getX();
		int ydelta = internalMouseEnd.getY() - internalMouseStart.getY();

		if (globals.getToolbarSettings().isSnapToGrid()) {
			xdelta -= xdelta % globals.getGridInternalWidth();
			ydelta -= ydelta % globals.getGridInternalWidth();
		}

		for (InternalObject internalObject : internalObjects) {
			if (internalObject instanceof InternalSegmentObject) {
				// TODO: if multiple WallSegmentObjects are to be moved and they are not in
				// collection, do not move just based on mouse - take into account space between
				// if more than one is on same wall
				if (internalObject instanceof WallSegmentObject
						&& !internalObjects.contains(((WallSegmentObject) internalObject).getAssociatedWall())) {

					WallSegmentObject wallSegmentObject = (WallSegmentObject) internalObject;
					moveWallSegment(internalMouseEnd.getDisplayPoint(globals), wallSegmentObject,
							wallSegmentObject.getAssociatedWall());
				} else {
					InternalSegmentObject internalSegmentObject = (InternalSegmentObject) internalObject;

					InternalPoint point1 = internalSegmentObject.getPoint1();
					InternalPoint point2 = internalSegmentObject.getPoint2();
					point1.setX(point1.getX() + xdelta);
					point1.setY(point1.getY() + ydelta);
					point2.setX(point2.getX() + xdelta);
					point2.setY(point2.getY() + ydelta);
				}
			} else if (internalObject instanceof ImageObject) {
				ImageObject imageObject = (ImageObject) internalObject;

				InternalPoint point = imageObject.getPoint();
				point.setX(point.getX() + xdelta);
				point.setY(point.getY() + ydelta);
			}
		}
	}

	/**
	 * Sets lower panel label message and mouse cursor based on state of program and
	 * button settings.
	 */
	private void setLabelMessageAndCursor() {
		if (globals.getButtonSettings().isAnyObjectBuildMode()) {
			if (globals.getButtonSettings().isBuildWallMode()) {
				changeCursor(SWT.CURSOR_HAND);

				if (globals.isBuildWallActive())
					globals.getController().getLabelMessage()
							.setText("Left click to save wall. Right click to cancel.");
				else
					globals.getController().getLabelMessage()
							.setText("Left click to begin building a wall. Right click to cancel.");
			} else if (globals.getButtonSettings().isBuildWindowMode()) {
				if (globals.isBuildWallSegmentObjectActive()) {
					changeCursor(SWT.CURSOR_HAND);
					globals.getController().getLabelMessage()
							.setText("Left click to save wall. Right click to cancel.");
				} else {
					changeCursor(SWT.CURSOR_ARROW);
					globals.getController().getLabelMessage()
							.setText("Left click near a wall to begin building a window. Right click to cancel.");
				}
			} else if (globals.getButtonSettings().isBuildDoorMode()) {
				if (globals.isBuildWallSegmentObjectActive()) {
					changeCursor(SWT.CURSOR_HAND);
					globals.getController().getLabelMessage()
							.setText("Left click to save door. Right click to cancel.");
				} else {
					changeCursor(SWT.CURSOR_ARROW);
					globals.getController().getLabelMessage()
							.setText("Left click near a wall to begin building a door. Right click to cancel.");
				}
			} else if (globals.getButtonSettings().isImageObjectBuildMode()) {
				if (globals.isBuildImageObjectActive()) {
					changeCursor(SWT.CURSOR_HAND);
					globals.getController().getLabelMessage().setText("Left click to save "
							+ globals.getBuildImageObject().getName() + ". Right click to cancel.");
				} else {
					changeCursor(SWT.CURSOR_ARROW);
					globals.getController().getLabelMessage().setText("");
				}
			}
		} else if (globals.getToolbarSettings().isPanMode()) {
			changeCursor(SWT.CURSOR_CROSS);
			globals.getController().getLabelMessage().setText("Hold left mouse and drag to pan.");
		} else if (globals.getToolbarSettings().isSelectMode()) {
			changeCursor(SWT.CURSOR_ARROW);
			globals.getController().getLabelMessage()
					.setText("Left click to select an object. Hold and drag to select multiple.");
		} else {
			changeCursor(SWT.CURSOR_ARROW);
			globals.getController().getLabelMessage().setText("");
		}
	}

	/**
	 * Changes the current mouse cursor.
	 * 
	 * @param mouseStyle SWT mouse cursor style
	 */
	private void changeCursor(int mouseStyle) {
		if (this.mouseStyle != mouseStyle) {
			// check for null e.g., for initialization and shell cursor is default system
			// cursor
			if (cursor != null)
				cursor.dispose();

			cursor = new Cursor(Display.getCurrent(), mouseStyle);
			globals.getController().getMainShell().setCursor(cursor);
			this.mouseStyle = mouseStyle;
		}
	}

}
