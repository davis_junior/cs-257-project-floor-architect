package main;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import components.ObjectLibrary;
import listeners.ButtonHandler;
import listeners.KeyHandler;
import listeners.MouseHandler;
import listeners.PaintHandler;
import objects.InternalObjectContext;
import settings.ButtonContext;
import settings.ColorContext;
import settings.FileContext;
import settings.GlobalContext;
import settings.ImageContext;
import settings.ToolbarContext;

/**
 * Controller is the root class of the application. It assembles all UI
 * components, including the canvas, tool bar, menu, and object library. The
 * main display loop is run in {@link #open()}.
 * 
 * @author D. Kornacki
 * @version 4-28-19
 */
public class Controller {

	private Shell shell;
	private GlobalContext globals;
	private ColorContext colorContext;
	private ImageContext imageContext;
	private FileContext fileContext;

	private Canvas canvas;
	private Label lblMessage;
	private Label lblCoords;

	private Menu menu;
	private ToolBar toolBar;

	/**
	 * Launch the application.
	 * 
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		try {
			Controller window = new Controller();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window, creating all contents. This method blocks until user closes.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		colorContext.dispose();
		imageContext.dispose();
	}

	/**
	 * Create contents of the window. Menu, tool bar, canvas, and object library are
	 * created here.
	 */
	private void createContents() {
		shell = new Shell();
		shell.setSize(1024, 768);
		shell.setMinimumSize(800, 600);
		resetTitle(null);
		shell.setLayout(new FormLayout());
		shell.addShellListener(new ShellAdapter() {

			@Override
			public void shellClosed(ShellEvent e) {
				e.doit = false;
				promptQuit();
			}
		});

		colorContext = new ColorContext();
		Color shellColor = colorContext.getCompositeColor();
		shell.setBackground(shellColor);

		imageContext = new ImageContext();

		createMenu();
		createToolbar();

		canvas = new Canvas(shell, SWT.DOUBLE_BUFFERED | SWT.NO_BACKGROUND);
		canvas.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		FormData fd_canvas = new FormData();
		fd_canvas.top = new FormAttachment(toolBar);
		fd_canvas.bottom = new FormAttachment(100, -30);
		fd_canvas.left = new FormAttachment(0);
		fd_canvas.right = new FormAttachment(100, -220);
		canvas.setLayoutData(fd_canvas);

		ScrolledComposite scrolledLibraryPanel = new ScrolledComposite(shell, SWT.BORDER | SWT.V_SCROLL);
		scrolledLibraryPanel.setLayout(new FillLayout());
		scrolledLibraryPanel.setExpandHorizontal(true);
		scrolledLibraryPanel.setExpandVertical(true);
		scrolledLibraryPanel.setAlwaysShowScrollBars(true);
		ObjectLibrary libraryPanel = new ObjectLibrary(scrolledLibraryPanel, SWT.NONE, colorContext);
		scrolledLibraryPanel.setContent(libraryPanel);
		scrolledLibraryPanel.setMinSize(libraryPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		Color libraryPanelColor = colorContext.getCompositeColor();
		scrolledLibraryPanel.setBackground(libraryPanelColor);
		FormData fd_libraryPanel = new FormData();
		fd_libraryPanel.top = new FormAttachment(toolBar);
		fd_libraryPanel.bottom = new FormAttachment(100, -30);
		fd_libraryPanel.right = new FormAttachment(100);
		fd_libraryPanel.left = new FormAttachment(canvas, 10);
		scrolledLibraryPanel.setLayoutData(fd_libraryPanel);

		Composite lowerPanel = new Composite(shell, SWT.NONE);
		Color compositeColor = colorContext.getCompositeColor();
		lowerPanel.setBackground(compositeColor);
		lowerPanel.setLayout(new FormLayout());
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(canvas);
		fd_composite.bottom = new FormAttachment(100);
		fd_composite.right = new FormAttachment(100);
		fd_composite.left = new FormAttachment(0);
		lowerPanel.setLayoutData(fd_composite);

		lblCoords = new Label(lowerPanel, SWT.NONE);
		FormData fd_lblCoords = new FormData();
		fd_lblCoords.top = new FormAttachment(0);
		fd_lblCoords.right = new FormAttachment(100, -10);
		lblCoords.setLayoutData(fd_lblCoords);
		lblCoords.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		lblCoords.setText("Coords:\t\t");

		lblMessage = new Label(lowerPanel, SWT.NONE);
		FormData fd_lblMessage = new FormData();
		fd_lblMessage.right = new FormAttachment(75);
		fd_lblMessage.top = new FormAttachment(0);
		fd_lblMessage.left = new FormAttachment(0, 10);
		lblMessage.setLayoutData(fd_lblMessage);
		lblMessage.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		lblMessage.setText("");

		canvas.setFocus();

		ToolbarContext toolbarSettings = new ToolbarContext(toolBar);
		ButtonContext buttonSettings = new ButtonContext(libraryPanel);
		InternalObjectContext internalObjectContext = new InternalObjectContext();
		fileContext = new FileContext(internalObjectContext, shell);
		globals = new GlobalContext(this, toolbarSettings, buttonSettings, colorContext, imageContext,
				internalObjectContext, fileContext);

		PaintHandler paintHandler = new PaintHandler(globals);
		canvas.addPaintListener(paintHandler);

		MouseHandler mouseHandler = new MouseHandler(globals);
		canvas.addMouseMoveListener(mouseHandler);
		canvas.addMouseListener(mouseHandler);
		canvas.addMouseTrackListener(mouseHandler);

		KeyHandler keyHandler = new KeyHandler(globals);
		canvas.addKeyListener(keyHandler);

		ButtonHandler buttonHandler = new ButtonHandler(globals);

		for (MenuItem topMenuItem : menu.getItems()) {
			for (MenuItem childItem : topMenuItem.getMenu().getItems()) {
				childItem.addSelectionListener(buttonHandler);

				if (childItem.getMenu() != null) {
					for (MenuItem child2Item : childItem.getMenu().getItems()) {
						child2Item.addSelectionListener(buttonHandler);
					}
				}
			}
		}

		for (ToolItem toolItem : toolBar.getItems())
			toolItem.addSelectionListener(buttonHandler);

		for (Control control : libraryPanel.getChildren()) {
			if (control instanceof Button) {
				((Button) control).addSelectionListener(buttonHandler);
			} else if (control instanceof Group) {
				for (Control groupControl : ((Group) control).getChildren()) {
					if (groupControl instanceof Button) {
						((Button) groupControl).addSelectionListener(buttonHandler);
					}
				}
			}
		}
	}

	/**
	 * Asks the user to save existing file if changed and not empty. Application
	 * then terminates.
	 * 
	 * @return <code>true</code> if application will terminate, <code>false</code>
	 *         otherwise
	 */
	public boolean promptQuit() {
		// skip prompt if empty file
		if (globals.getObjects().getAllObjects(true).length == 0 && fileContext.getFile() == null) {
			shell.dispose();
			return true;
		}

		int result = fileContext.showSaveFileMessageBox("Exit Application");
		if (result != SWT.CANCEL) {
			if (result == SWT.YES) {
				if (!fileContext.save())
					return false;
			}

			shell.dispose();
			return true;
		}

		return false;
	}

	/**
	 * Create the menu.
	 */
	private void createMenu() {
		menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);

		MenuItem mntmFile = new MenuItem(menu, SWT.CASCADE);
		mntmFile.setText("&File");
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		mntmFile.setMenu(fileMenu);
		MenuItem mntmNew = new MenuItem(fileMenu, SWT.CASCADE);
		mntmNew.setText("&New\tCtrl+N");
		mntmNew.setAccelerator(SWT.CTRL + 'N');
		mntmNew.setData("command", "new");
		MenuItem mntmOpen = new MenuItem(fileMenu, SWT.CASCADE);
		mntmOpen.setText("&Open File...\tCtrl+O");
		mntmOpen.setAccelerator(SWT.CTRL + 'O');
		mntmOpen.setData("command", "open_file");
		MenuItem mntmSave = new MenuItem(fileMenu, SWT.CASCADE);
		mntmSave.setText("&Save\tCtrl+S");
		mntmSave.setAccelerator(SWT.CTRL + 'S');
		mntmSave.setData("command", "save");
		MenuItem mntmSaveAs = new MenuItem(fileMenu, SWT.CASCADE);
		mntmSaveAs.setText("Save &As...");
		mntmSaveAs.setData("command", "save_as");
		MenuItem mntmQuit = new MenuItem(fileMenu, SWT.CASCADE);
		mntmQuit.setText("&Quit\tCtrl+Q");
		mntmQuit.setAccelerator(SWT.CTRL + 'Q');
		mntmQuit.setData("command", "quit");

//		MenuItem mntmEdit = new MenuItem(menu, SWT.CASCADE);
//		mntmEdit.setText("&Edit");
//		Menu editMenu = new Menu(shell, SWT.DROP_DOWN);
//		mntmEdit.setMenu(editMenu);

		MenuItem mntmView = new MenuItem(menu, SWT.CASCADE);
		mntmView.setText("&View");
		Menu viewMenu = new Menu(shell, SWT.DROP_DOWN);
		mntmView.setMenu(viewMenu);
		MenuItem mntmZoomIn = new MenuItem(viewMenu, SWT.CASCADE);
		mntmZoomIn.setText("Zoom &In\tCtrl+Plus");
		mntmZoomIn.setAccelerator(SWT.CTRL + '=');
		mntmZoomIn.setData("command", "zoom_in");
		MenuItem mntmZoomOut = new MenuItem(viewMenu, SWT.CASCADE);
		mntmZoomOut.setText("Zoom &Out\tCtrl+Minus");
		mntmZoomOut.setAccelerator(SWT.CTRL + '-');
		mntmZoomOut.setData("command", "zoom_out");

		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("&Help");
		Menu helpMenu = new Menu(shell, SWT.DROP_DOWN);
		mntmHelp.setMenu(helpMenu);
		MenuItem mntmAbout = new MenuItem(helpMenu, SWT.CASCADE);
		mntmAbout.setText("&About");
		mntmAbout.setData("command", "about");
	}

	/**
	 * Create the toolbar.
	 */
	private void createToolbar() {
		toolBar = new ToolBar(shell, SWT.FLAT | SWT.WRAP | SWT.HORIZONTAL);
		Color toolbarColor = colorContext.getToolbarColor();
		toolBar.setBackground(toolbarColor);
		FormData fd_toolBar = new FormData();
		fd_toolBar.right = new FormAttachment(100);
		fd_toolBar.left = new FormAttachment(0);
		fd_toolBar.top = new FormAttachment(0);
		toolBar.setLayoutData(fd_toolBar);

		createToolbarItem(SWT.PUSH, "New", "new", false, "new.png", "Create new project");
		createToolbarItem(SWT.PUSH, "Open", "open", false, "open.png", "Open project file");
		createToolbarItem(SWT.PUSH, "Save", "save", false, "save.png", "Save project to file");
		new ToolItem(toolBar, SWT.SEPARATOR);
		createToolbarItem(SWT.RADIO, "Pan", "pan_mode", true, "pan.png", "Pan screen");
		createToolbarItem(SWT.RADIO, "Select", "select_mode", false, "select.png", "Select objects");
		new ToolItem(toolBar, SWT.SEPARATOR);
		createToolbarItem(SWT.CHECK, "Snap-to-Grid", "snap_to_grid", true, "snaptogrid.png",
				"Make actions on objects snap-to-grid");
		createToolbarItem(SWT.CHECK, "Show Grid", "show_grid", true, "showgrid.png", "Show the grid");
		new ToolItem(toolBar, SWT.SEPARATOR);
		createToolbarItem(SWT.CHECK, "Show Measurements", "show_measurements", true, "showmeasurements.png",
				"Show measurement labels");
		new ToolItem(toolBar, SWT.SEPARATOR);
		createToolbarItem(SWT.PUSH, "Zoom In", "zoom_in", false, "zoomin.png", "Zoom in on screen");
		createToolbarItem(SWT.PUSH, "Zoom Out", "zoom_out", false, "zoomout.png", "Zoom out on screen");
	}

	/**
	 * Create a single tool item under the tool bar.
	 * 
	 * @param style         SWT style
	 * @param name          Name
	 * @param command       Command
	 * @param selected      Whether the tool item should be set selected
	 * @param imageFileName Image file name
	 */
	private void createToolbarItem(int style, String name, String command, boolean selected, String imageFileName,
			String toolTip) {

		ToolItem tltm = new ToolItem(toolBar, style);
		Image originalImage = new Image(Display.getCurrent(),
				this.getClass().getClassLoader().getResourceAsStream("images/toolbar/" + imageFileName));
		Image sclaedImage = new Image(Display.getCurrent(), originalImage.getImageData().scaledTo(40, 40));
		GC gc = new GC(sclaedImage);
		gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
		gc.fillRectangle(sclaedImage.getBounds());
		gc.dispose();
		tltm.setImage(sclaedImage);
		tltm.setText(name);
		tltm.setData("command", command);
		tltm.setToolTipText(toolTip);

		if (style == SWT.CHECK || style == SWT.RADIO)
			tltm.setSelection(selected);
	}

	/**
	 * Gets the main application shell.
	 * 
	 * @return Shell
	 */
	public Shell getMainShell() {
		return shell;
	}

	/**
	 * Gets the canvas.
	 * 
	 * @return Canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * Gets the message label from the lower panel.
	 * 
	 * @return Message Label
	 */
	public Label getLabelMessage() {
		return lblMessage;
	}

	/**
	 * Gets the coordinate label from the lower panel.
	 * 
	 * @return Coordinate label
	 */
	public Label getLabelCoords() {
		return lblCoords;
	}

	/**
	 * Sets the application title to fileName, appending application name.
	 * 
	 * @param fileName File name
	 */
	public void resetTitle(String fileName) {
		if (fileName == null || fileName.trim().isEmpty())
			shell.setText("Untitled - Floor Architect");
		else
			shell.setText(fileName + " - Floor Architect");
	}
}
