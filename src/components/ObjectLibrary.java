package components;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import settings.ColorContext;

/**
 * ObjectLibrary is the right side panel of the application that includes all
 * the possible build options as buttons. Selecting a button will allow the
 * building of the respective object.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ObjectLibrary extends Composite {

	private ColorContext colorContext;

	public ObjectLibrary(Composite parent, int style, ColorContext colorContext) {
		super(parent, style);

		this.colorContext = colorContext;

		createContents();
	}

	/**
	 * Creates the contents. Labels, groups, and buttons with images are created.
	 */
	private void createContents() {
		setBackground(colorContext.getCompositeColor());
		setLayout(new FormLayout());

		Label lblObjectLibrary = new Label(this, SWT.NONE);
		FormData fd_lblObjectLibrary = new FormData();
		fd_lblObjectLibrary.top = new FormAttachment(0, 5);
		fd_lblObjectLibrary.left = new FormAttachment(0, 5);
		lblObjectLibrary.setLayoutData(fd_lblObjectLibrary);
		lblObjectLibrary.setText("Library");
		lblObjectLibrary.setBackground(colorContext.getCompositeColor());

		Group grpWallObjects = new Group(this, SWT.NONE);
		grpWallObjects.setLayout(new RowLayout(SWT.VERTICAL));
		FormData fd_grpWallObjects = new FormData();
		fd_grpWallObjects.top = new FormAttachment(lblObjectLibrary, 15);
		fd_grpWallObjects.left = new FormAttachment(0, 5);
		grpWallObjects.setLayoutData(fd_grpWallObjects);
		grpWallObjects.setText("Wall");
		grpWallObjects.setBackground(colorContext.getCompositeColor());

		Button btnWall = new Button(grpWallObjects, SWT.TOGGLE | SWT.FLAT);
		Image originalWallImage = new Image(Display.getCurrent(),
				getClass().getClassLoader().getResourceAsStream("images/library/wall/wall.png"));
		Image scaledWallImage = new Image(Display.getCurrent(), originalWallImage.getImageData().scaledTo(50, 50));
		GC gcWall = new GC(scaledWallImage);
		gcWall.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
		gcWall.fillRectangle(scaledWallImage.getBounds());
		gcWall.dispose();
		btnWall.setImage(scaledWallImage);
		btnWall.setText("Wall");
		btnWall.setBackground(colorContext.getCompositeColor());
		btnWall.setLayoutData(new RowData(160, 75));
		btnWall.setData("command", "wall_build_mode");
		btnWall.setData("name", "wall");
		btnWall.setData("group", "wall");
		btnWall.setToolTipText("Build a wall");

		Button btnWindow = new Button(grpWallObjects, SWT.TOGGLE | SWT.FLAT);
		Image originalWindowImage = new Image(Display.getCurrent(),
				getClass().getClassLoader().getResourceAsStream("images/library/wall/window.png"));
		Image scaledWindowImage = new Image(Display.getCurrent(), originalWindowImage.getImageData().scaledTo(50, 50));
		GC gcWindow = new GC(scaledWindowImage);
		gcWindow.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
		gcWindow.fillRectangle(scaledWindowImage.getBounds());
		gcWindow.dispose();
		btnWindow.setImage(scaledWindowImage);
		btnWindow.setText("Window");
		btnWindow.setBackground(colorContext.getCompositeColor());
		btnWindow.setLayoutData(new RowData(160, 75));
		btnWindow.setData("command", "window_build_mode");
		btnWindow.setData("name", "window");
		btnWindow.setData("group", "wall");
		btnWindow.setToolTipText("Build a window");

		Button btnDoor = new Button(grpWallObjects, SWT.TOGGLE | SWT.FLAT);
		Image originalDoorImage = new Image(Display.getCurrent(),
				getClass().getClassLoader().getResourceAsStream("images/library/wall/door.png"));
		Image sclaedDoorImage = new Image(Display.getCurrent(), originalDoorImage.getImageData().scaledTo(50, 50));
		GC gcDoor = new GC(sclaedDoorImage);
		gcDoor.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
		gcDoor.fillRectangle(scaledWindowImage.getBounds());
		gcDoor.dispose();
		btnDoor.setImage(sclaedDoorImage);
		btnDoor.setText("Door");
		btnDoor.setBackground(colorContext.getCompositeColor());
		btnDoor.setLayoutData(new RowData(160, 75));
		btnDoor.setData("command", "door_build_mode");
		btnDoor.setData("name", "door");
		btnDoor.setData("group", "wall");
		btnDoor.setToolTipText("Build a door");

		Group grpFurniture = new Group(this, SWT.NONE);
		grpFurniture.setLayout(new RowLayout(SWT.VERTICAL));
		FormData fd_grpFurniture = new FormData();
		fd_grpFurniture.top = new FormAttachment(grpWallObjects, 10);
		fd_grpFurniture.left = new FormAttachment(0, 5);
		grpFurniture.setLayoutData(fd_grpFurniture);
		grpFurniture.setText("Furniture");
		grpFurniture.setBackground(colorContext.getCompositeColor());

		Group grpAppliance = new Group(this, SWT.NONE);
		grpAppliance.setLayout(new RowLayout(SWT.VERTICAL));
		FormData fd_grpAppliance = new FormData();
		fd_grpAppliance.top = new FormAttachment(grpFurniture, 10);
		fd_grpAppliance.left = new FormAttachment(0, 5);
		grpAppliance.setLayoutData(fd_grpAppliance);
		grpAppliance.setText("Appliance");
		grpAppliance.setBackground(colorContext.getCompositeColor());

		Group grpUtility = new Group(this, SWT.NONE);
		grpUtility.setLayout(new RowLayout(SWT.VERTICAL));
		FormData fd_grpUtility = new FormData();
		fd_grpUtility.top = new FormAttachment(grpAppliance, 10);
		fd_grpUtility.left = new FormAttachment(0, 5);
		grpUtility.setLayoutData(fd_grpUtility);
		grpUtility.setText("Utility");
		grpUtility.setBackground(colorContext.getCompositeColor());

		addButtonToGroup(grpFurniture, "Furniture", "images/library/furniture");
		addButtonToGroup(grpAppliance, "Appliance", "images/library/appliance");
		addButtonToGroup(grpUtility, "Utility", "images/library/utility");

	}

	/**
	 * Add a button to the specified group.
	 * 
	 * @param group     Group
	 * @param groupName Name of group
	 * @param imagePath Path to images
	 */
	private void addButtonToGroup(Group group, String groupName, String imageParentPath) {
		Map<String, String> imagePathsToNames = new HashMap<>();

		// check if application loaded from jar
		// cannot use normal File operations if jar
		File jarFile = null;
		try {
			jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
		}

		if (jarFile != null && jarFile.isFile()) {
			JarFile jar = null;
			try {
				jar = new JarFile(jarFile);
				Enumeration<JarEntry> entries = jar.entries();
				while (entries.hasMoreElements()) {
					String entryPath = entries.nextElement().getName();
					if (entryPath.startsWith(imageParentPath) && entryPath.endsWith(".png")) {
						String imagePath = entryPath;
						String imageFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
						imagePathsToNames.put(imagePath, imageFileName);
					}
				}
			} catch (IOException e) {
			} finally {
				if (jar != null)
					try {
						jar.close();
					} catch (IOException e) {
					}
			}
		} else {
			InputStream imagesStream = getClass().getClassLoader().getResourceAsStream(imageParentPath);
			BufferedReader br = new BufferedReader(new InputStreamReader(imagesStream));

			String imageFileName;
			try {
				while ((imageFileName = br.readLine()) != null) {
					String imagePath = imageParentPath + "/" + imageFileName;
					imagePathsToNames.put(imagePath, imageFileName);
				}
			} catch (IOException e) {
			}
		}

		for (String imagePath : imagePathsToNames.keySet()) {
			String imageFileName = imagePathsToNames.get(imagePath);

			Button btn = new Button(group, SWT.TOGGLE | SWT.FLAT);
			Image originalImage = new Image(Display.getCurrent(),
					getClass().getClassLoader().getResourceAsStream(imagePath));
			Image sclaedImage = new Image(Display.getCurrent(), originalImage.getImageData().scaledTo(50, 50));
			GC gc = new GC(sclaedImage);
			gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_BLUE));
			gc.fillRectangle(sclaedImage.getBounds());
			gc.dispose();
			btn.setImage(sclaedImage);
			String lowerCaseName = imageFileName.toLowerCase().substring(0, imageFileName.lastIndexOf("."));
			String firstLetterUpperName = lowerCaseName.substring(0, 1).toUpperCase() + lowerCaseName.substring(1);
			btn.setText(firstLetterUpperName);
			btn.setBackground(colorContext.getCompositeColor());
			btn.setLayoutData(new RowData(160, 75));
			btn.setData("command", lowerCaseName + "_image_build_mode");
			btn.setData("name", lowerCaseName);
			btn.setData("group", groupName.toLowerCase());
			btn.setData("path", imagePath.replace("\\", "/").replace("/library/", "/paint/"));

			String indefiniteArticle = "a";
			if (lowerCaseName.startsWith("a") || lowerCaseName.startsWith("e") || lowerCaseName.startsWith("i")
					|| lowerCaseName.startsWith("o") || lowerCaseName.startsWith("u"))
				indefiniteArticle = "an";

			btn.setToolTipText("Build " + indefiniteArticle + " " + lowerCaseName);
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
