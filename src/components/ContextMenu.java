package components;

import java.util.Collection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import objects.InternalObject;
import settings.GlobalContext;

/**
 * ContextMenu creates a popup menu for the specified InternalObjects. Options
 * currently include Delete. Popup menu is spawned at current mouse coordinates.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class ContextMenu {

	private Collection<InternalObject> internalObjects;
	private GlobalContext globals;

	private Menu menu;

	public ContextMenu(Collection<InternalObject> internalObjects, GlobalContext globals) {
		this.internalObjects = internalObjects;
		this.globals = globals;

		this.menu = new Menu(globals.getController().getCanvas());
		initialize();
	}

	/**
	 * Creates menu items.
	 */
	private void initialize() {
		MenuItem deleteItem = new MenuItem(menu, SWT.PUSH);
		deleteItem.setText("Delete");
		deleteItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				globals.getObjects().removeObjects(internalObjects);

				globals.getClosestObjects().clear();
				globals.getSelectedObjects().clear();

				globals.getController().getCanvas().redraw();
			}
		});
	}

	/**
	 * Opens the popup menu.
	 */
	public void open() {
		menu.setLocation(Display.getCurrent().getCursorLocation());
		menu.setVisible(true);
	}
}
