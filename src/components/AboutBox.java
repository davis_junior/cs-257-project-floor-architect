package components;

import java.awt.Desktop;
import java.net.URI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationAdapter;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * AboutBox is a simple informational dialog box spawned from the about menu
 * item.
 * 
 * @author D. Kornacki
 * @version 4-30-2019
 */
public class AboutBox extends Dialog {

	private Shell shell;

	/**
	 * Create the about box.
	 * 
	 * @param parent
	 * @param style
	 */
	public AboutBox(Shell parent, int style) {
		super(parent, style | SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
		setText("About");
	}

	/**
	 * Open the about box.
	 * 
	 * @return the result
	 */
	public void open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the about box.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(600, 300);
		shell.setText(getText());

		Browser browser = new Browser(shell, SWT.NONE);
		// get default shell background color to replicate in browser
		RGB rgb = shell.getBackground().getRGB();
		browser.setBounds(10, 10, 574, 192);
		browser.addLocationListener(new LocationAdapter() {

			@Override
			public void changing(LocationEvent event) {
				// redirect all urls to open system browser rather than this browser object
				if (!event.location.equals("about:blank") && !event.location.trim().isEmpty()) {
					event.doit = false;
					if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
						try {
							Desktop.getDesktop().browse(new URI(event.location));
						} catch (Exception e) {
						}
					}
				}
			}
		});
		browser.setText("<html><style type=\"text/css\">body { background-color: rgb(" + rgb.red + ", " + rgb.green
				+ ", " + rgb.blue
				+ "); } tab { padding-left: 1em; }</style><body><b>Author:</b> David Kornacki<br /><tab>- Computer Science student</tab><br /><tab> @ St. Bonaventure University</tab><br /><br /><b>Floor Architect</b> is used to create floor layouts for residential buildings. Currently, walls, windows, doors, and some furniture, utilities, and appliances can be placed.<br /><br/ ><b>Feature Overview</b><br /><tab>- Click on object button to begin placement</tab><br /><tab>- Group select objects by clicking and dragging mouse in selection mode</tab><br /><tab>- Move objects by selecting them first. Then while holding the mouse down, dragging them</tab><br /><br /><b>Credits to:</b><br /><tab>-<a href=\"https://www.flaticon.com/\">Freepik Company</a> for various icon images</tab><br><tab>-<a href=\"https://thenounproject.com/\">Noun Project</a> for various image icons</tab><br /><tab>- <a href = \"https://cad-block.com/\">CAD-block.com</a> for various top-down images of objects</tab></body></html>");

		Button btnOk = new Button(shell, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.dispose();
			}
		});
		btnOk.setBounds(249, 215, 105, 35);
		btnOk.setText("Ok");

	}
}
