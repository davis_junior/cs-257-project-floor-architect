package coords;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import settings.GlobalContext;
import utility.GeometricCalculations;

/**
 * InternalPoint represents coordinates in the internal point system of the
 * application.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
@XmlRootElement(name = "Point")
@XmlAccessorType(XmlAccessType.FIELD)
public class InternalPoint {

	@XmlElement(name = "X")
	private int x;

	@XmlElement(name = "Y")
	private int y;

	public InternalPoint() {
		this.x = 0;
		this.y = 0;
	}

	public InternalPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public InternalPoint(InternalPoint internalPoint) {
		this.x = internalPoint.x;
		this.y = internalPoint.y;
	}

	/**
	 * Gets X coordinate.
	 * 
	 * @return X coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets X coordinate.
	 * 
	 * @param x X coordinate
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Gets Y coordinate.
	 * 
	 * @return Y coordinate
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets Y coordinate.
	 * 
	 * @param y Y coordinate
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Gets mathematical distance from this point to the specified point.
	 * 
	 * @param point Other point
	 * @return Distance from this point to the specified point
	 */
	public double getDistanceTo(InternalPoint point) {
		return GeometricCalculations.getDistance(this, point);
	}

	/**
	 * Gets the distance in feet from this point to the specified point.
	 * 
	 * @param point   Other point
	 * @param globals GlobalContext
	 * @return Distance in feet from this pont to the specified point
	 */
	public double getDistanceToInFeet(InternalPoint point, GlobalContext globals) {
		return GeometricCalculations.getDistanceInFeet(this, point, globals);
	}

	/**
	 * Gets the distance in inches from this point to the specified point.
	 * 
	 * @param point   Other point
	 * @param globals GlobalContext
	 * @return Distance in inches from this point to the specified point
	 */
	public double getDistanceToInInches(InternalPoint point, GlobalContext globals) {
		return GeometricCalculations.getDistanceInInches(this, point, globals);
	}

	/**
	 * Gets a formatted distance from this point to the specified point. The format
	 * is to allow for 2 decimal places only.
	 * 
	 * @param point   Other point
	 * @param globals GlobalContext
	 * @return Formatted distance String from this point to the specified point
	 */
	public String getDistanceToString(InternalPoint point, GlobalContext globals) {
		return GeometricCalculations.getDistanceString(this, point, globals);
	}

	/**
	 * Gets the screen coordinates calculated from this point.
	 * 
	 * @param globals GlobalContext
	 * @return DisplayPoint - screen coordinates
	 */
	public DisplayPoint getDisplayPoint(GlobalContext globals) {
		int displayX = (int) ((this.getX() - globals.getBasePoint().getX()) * globals.getZoomFactor());
		int displayY = (int) ((this.getY() - globals.getBasePoint().getY()) * globals.getZoomFactor());

		return new DisplayPoint(displayX, displayY);
	}

	/**
	 * Gets the closest internal grid point. A grid point is where grid lines
	 * intersect.
	 * 
	 * @param globals GlobalContext
	 * @return Closest grid InternalPoint
	 */
	public InternalPoint getNearestGridPoint(GlobalContext globals) {
		int remainderx = x % globals.getGridInternalWidth();
		int remaindery = y % globals.getGridInternalWidth();

		int resultx = x;
		int resulty = y;

		if (remainderx > globals.getGridInternalWidth() / 2)
			resultx += globals.getGridInternalWidth();

		if (remaindery > globals.getGridInternalWidth() / 2)
			resulty += globals.getGridInternalWidth();

		return new InternalPoint(resultx - remainderx, resulty - remaindery);
	}

}
