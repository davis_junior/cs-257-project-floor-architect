package coords;

import settings.GlobalContext;
import utility.GeometricCalculations;

/**
 * DisplayPoint is a coordinate object associated only with relative screen
 * coordinates.
 * 
 * @author D. Kornacki
 * @version 4-28-2019
 */
public class DisplayPoint {

	private int x;
	private int y;

	public DisplayPoint() {
		this.x = 0;
		this.y = 0;
	}

	public DisplayPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public DisplayPoint(DisplayPoint displayPoint) {
		this.x = displayPoint.x;
		this.y = displayPoint.y;
	}

	/**
	 * Gets X coordinate.
	 * 
	 * @return X coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets X coordinate.
	 * 
	 * @param x X coordinate
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Gets Y coordinate.
	 * 
	 * @return Y coordinate
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets Y coordinate.
	 * 
	 * @param y Y coordinate
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Gets mathematical distance from this point to the specified point.
	 * 
	 * @param point Other point
	 * @return Distance from this point to the specified point
	 */
	public double getDistanceTo(DisplayPoint point) {
		return GeometricCalculations.getDistance(this, point);
	}

	/**
	 * Calculates the internal point from this display point based on global
	 * settings.
	 * 
	 * @param globals GlobalContext
	 * @return InternalPoint
	 */
	public InternalPoint getInternalPoint(GlobalContext globals) {
		int internalX = (int) (globals.getBasePoint().getX() + this.x * globals.getInverseZoomFactor());
		int internalY = (int) (globals.getBasePoint().getY() + this.y * globals.getInverseZoomFactor());

		return new InternalPoint(internalX, internalY);
	}
}
